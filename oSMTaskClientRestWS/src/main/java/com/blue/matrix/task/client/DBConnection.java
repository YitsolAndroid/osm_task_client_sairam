
package com.blue.matrix.task.client;

import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.blue.matrix.task.client.exception.GenericAllException;

public final class DBConnection {
	
	final static Logger logger = Logger.getLogger(DB_Connection.class);
	private static DBConnection connection = null;
	private static String DRIVER_NAME;
	private static String CONNECTION_URL;
	private static String DB_USER_NAME;
	private static String DB_PASSWORD;
	
	static {
		
		try {
			
			final Properties props = new Properties();
			props.load(Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("/database.properties"));
			DRIVER_NAME = props.getProperty("database.driver");
			CONNECTION_URL = props.getProperty("database.url");
			DB_USER_NAME = props.getProperty("database.user");
			DB_PASSWORD = props.getProperty("database.password");
			
		}
		catch (IOException e) {
			
			throw new GenericAllException(Messages.getString("DB_Connection.0")); //$NON-NLS-1$
			
		}
	}
	
	private DBConnection ( ) {
	
	}
	
	public static synchronized DBConnection getDBConnectionInstance() {
	
		if (connection == null) {
			connection = new DBConnection();
			return connection;
		}
		else {
			return connection;
		}
		
	}
	
	public static String getDRIVER_NAME() {
	
		return DRIVER_NAME;
	}
	
	public static void setDRIVER_NAME(String dRIVER_NAME) {
	
		DRIVER_NAME = dRIVER_NAME;
	}
	
	public static String getCONNECTION_URL() {
	
		return CONNECTION_URL;
	}
	
	public static void setCONNECTION_URL(String cONNECTION_URL) {
	
		CONNECTION_URL = cONNECTION_URL;
	}
	
	public static String getDB_USER_NAME() {
	
		return DB_USER_NAME;
	}
	
	public static void setDB_USER_NAME(String dB_USER_NAME) {
	
		DB_USER_NAME = dB_USER_NAME;
	}
	
	public static String getDB_PASSWORD() {
	
		return DB_PASSWORD;
	}
	
	public static void setDB_PASSWORD(String dB_PASSWORD) {
	
		DB_PASSWORD = dB_PASSWORD;
	}
	
}
