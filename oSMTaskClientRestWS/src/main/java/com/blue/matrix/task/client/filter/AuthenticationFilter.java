
package com.blue.matrix.task.client.filter;

/**
 *  @author Jaya sankar
 */
import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.ext.Provider;
import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;

import com.blue.matrix.task.client.encryption.Encryption;
import com.blue.matrix.task.client.exception.CredentialsException;

/**
 * 
 * @author jaya sankar
 *
 */
@Provider
@PreMatching
public class AuthenticationFilter implements ContainerRequestFilter {
	
	final static Logger logger = Logger.getLogger(AuthenticationFilter.class);
	
	public static final String AUTHENTICATION_HEADER = "Auth";
	
	@Override
	public void filter(ContainerRequestContext containerRequest) throws IOException {
	
		Encryption encryption = new Encryption();
		
		String authorization = containerRequest.getHeaderString(AUTHENTICATION_HEADER);
		if (authorization != null) {
			// byte[] message =
			// authorization.substring("o1s2m3".length()).trim().getBytes();
			try {
				
				byte[] decoded = DatatypeConverter.parseBase64Binary(authorization.trim());
				String decodedSt = new String(decoded);
				String[] parts = decodedSt.split(" ");
				if (!parts[0].equals("Basic")) {
					throw new CredentialsException(
							"{\"errorCode\":401,\"errorMsg\":\"Key  not found!\"}");
					
				}
				
				String decodedString = encryption.decrypt(parts[1].trim());
				String[] actualCredentials = decodedString.split(":");
				
				String username = actualCredentials[0];
				String password = actualCredentials[1];
				
				containerRequest.setProperty("password", password);
				containerRequest.setProperty("username", username);
				
				// return containerRequest;
				if (username == null) {
					
					logger.info("------------------ User name is null -----------------");
					throw new CredentialsException(
							"{\"errorCode\":401,\"errorMsg\":\"Username  not found!\"}");
					
				}
				else if (password == null) {
					
					logger.info("------------------ Password is null -----------------");
					throw new CredentialsException(
							"{\"errorCode\":401,\"errorMsg\":\"Password  not found!\"}");
					
				}
				
			}
			catch (Exception e) {
				
				throw new CredentialsException(
						"{\"errorCode\":401,\"errorMsg\":\"key  not found!\"}");
			}
		}
		
		else {
			
			logger.info("Authorization : failure");
			throw new CredentialsException("{\"errorCode\":401,\"errorMsg\":\"Auth  not found!\"}");
		}
	}
	
}
