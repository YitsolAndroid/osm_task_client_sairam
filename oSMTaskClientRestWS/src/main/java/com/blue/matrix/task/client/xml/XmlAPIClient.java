
package com.blue.matrix.task.client.xml;

/**
 * 
 * @author jaya sankar
 * @Descrption This class contains different methods which will interact with
 *             OSM XML Web Services.
 *
 */
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.namespace.QName;

import org.apache.log4j.Logger;

import com.blue.matrix.task.client.model.CopyOrder;
import com.blue.matrix.task.client.model.FalloutTaskRequest;
import com.blue.matrix.task.client.model.GetNextOrderAtTask;
import com.blue.matrix.task.client.model.GetOrderAtTaskRequest;
import com.blue.matrix.task.client.model.GetOrderDataHistoryRequest;
import com.blue.matrix.task.client.model.GetOrderProcessHistoryRequest;
import com.blue.matrix.task.client.model.GetOrderRequest;
import com.blue.matrix.task.client.model.GetOrderStateHistoryRequest;
import com.blue.matrix.task.client.model.GetTaskStatusesRequest;
import com.blue.matrix.task.client.model.GetUserInfoRequest;
import com.blue.matrix.task.client.model.ListExceptionsRequest;
import com.blue.matrix.task.client.model.ListViewsRequest;
import com.blue.matrix.task.client.model.ModifyRemark;
import com.blue.matrix.task.client.model.NotificationsRequest;
import com.blue.matrix.task.client.model.OrderTypesNSourcesRequest;
import com.blue.matrix.task.client.model.ReceiveOrder;
import com.blue.matrix.task.client.model.SetException;
import com.blue.matrix.task.client.model.TaskDescription;

public class XmlAPIClient {
	
	final static Logger logger = Logger.getLogger(XmlAPIClient.class);
	
	public Boolean connectivity(String server, String port) {
	
		Boolean result = true;
		
		try {
			URL url = new URL("http://" + server + ":" + port + "/OrderManagement/");
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setConnectTimeout(1000);
			
			if (connection.getHeaderField(0) == null) {
				result = false;
			}
			else {
				result = true;
			}
		}
		catch (Exception e) {
			result = false;
		}
		
		return result;
	}
	
	public String getLoginData(String localIP, String port, String username, String password) {
	
		String strResponse = "failure";
		int loginSuccess = 1;
		@SuppressWarnings("deprecation")
		String str = URLEncoder.encode("username") + "=" + URLEncoder.encode(username) + "&"
				+ URLEncoder.encode("password") + "=" + URLEncoder.encode(password);
		
		boolean connectivity = connectivity(localIP, port);
		if (connectivity == true) {
			
			try {
				byte[] bytes = str.getBytes();
				URL url = new URL("http://" + localIP + ":" + port
						+ "/OrderManagement/XMLAPI/login");
				HttpURLConnection connection = (HttpURLConnection) url.openConnection();
				connection.setConnectTimeout(1000);
				connection.setAllowUserInteraction(false);
				connection.setDoOutput(true);
				connection.setDoInput(true);
				connection.setRequestMethod("POST");
				connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
				connection.setRequestProperty("Content-Length", String.valueOf(bytes.length));
				connection.connect();
				OutputStream out = connection.getOutputStream();
				out.write(bytes);
				out.close();
				
				int code = connection.getResponseCode();
				String receivedcookie = null;
				if (code == HttpURLConnection.HTTP_OK) {
					loginSuccess = 1;
					receivedcookie = connection.getHeaderField("Set-Cookie");
					if (receivedcookie == null) {
						loginSuccess = 2;
						return "failure";
					}
				}
				else {
					loginSuccess = 0;
					return "failure";
				}
				connection.disconnect();
				
				if (loginSuccess == 1) {
					
					String key = receivedcookie.substring(0, receivedcookie.indexOf(";"));
					return key;
				}
			}
			catch (Exception e) {
				loginSuccess = 0;
				return "failure";
			}
			
		}
		else {
			return "wport";
		}
		
		return strResponse;
	}
	
	/**
	 * This Web services Method@Descrption will find out weather user is
	 * available in OSM , if user is available it will return cookie
	 * 
	 * @param publicIP
	 * @param port
	 * @param username
	 * @param password
	 * @return
	 */
	
	public String logOut(String publicIP, String port, String username, String password) {
	
		String str = (new StringBuilder(String.valueOf(URLEncoder.encode("username")))).append("=")
				.append(URLEncoder.encode(username)).append("&")
				.append(URLEncoder.encode("password")).append("=")
				.append(URLEncoder.encode(password)).toString();
		URL url;
		HttpURLConnection connection = null;
		try {
			String headerValue = logoutBuilder(publicIP, port, str);
			if (headerValue == null) {
				return "failure";
				
			}
			else {
				
				return "success";
			}
			
		}
		catch (MalformedURLException e1) {
			logger.error(e1);
			return "failure";
		}
		catch (IOException e) {
			logger.error(e);
			return "failure";
		}
		catch (Exception e) {
			logger.error(e);
			return "failure";
		}
		
	}

	/**
	 * @param publicIP
	 * @param port
	 * @param str
	 * @return
	 * @throws MalformedURLException
	 * @throws IOException
	 * @throws ProtocolException
	 */
	private String logoutBuilder(String publicIP, String port, String str)
			throws MalformedURLException, IOException, ProtocolException {
	
		URL url;
		HttpURLConnection connection;
		url = new URL((new StringBuilder("http://")).append(publicIP).append(":").append(port)
				.append("/OrderManagement/XMLAPI/login").append("?" + str).toString());
		connection = (HttpURLConnection) url.openConnection();
		connection.setAllowUserInteraction(false);
		connection.setDoOutput(true);
		connection.setDoInput(true);
		String headerValue = null;
		connection.setRequestMethod("POST");
		connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		connection.setRequestProperty("Content-Length", "0");
		connection.connect();
		String response = connection.getResponseMessage();
		headerValue = connection.getHeaderField("Set-Cookie");
		return headerValue;
	}
	
	/**
	 * 
	 * @param publicIP
	 * @param port
	 * @param username
	 * @param password
	 * @param ro
	 * @return
	 */
	public String taskDescription(String publicIP, String port, String username, String password,
			TaskDescription ro) {
	
		URL url;
		String cookiee = getLoginData(publicIP, port, username, password);
		JAXBElement<TaskDescription> element = new JAXBElement<TaskDescription>(new QName(
				"urn:com:metasolv:oms:xmlapi:1", "TaskDescription.Request"), TaskDescription.class,
				ro);
		
		StringWriter stringWirter = null;
		String message = null;
		try {
			final Marshaller marashaller = JAXBContext.newInstance(TaskDescription.class)
					.createMarshaller();
			marashaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			stringWirter = new StringWriter();
			marashaller.marshal(element, stringWirter);
			String urlPath = urlBuilder(publicIP, port);
			message = xmlApiCall(cookiee, urlPath, stringWirter.toString());
			logger.info("Response : " + message);
		}
		catch (PropertyException e1) {
			logger.error(e1);
		}
		catch (JAXBException e) {
			logger.error(e);
		}
		logger.info("Response from OSM XML:" + message);
		return message;
	}
	
	/**
	 * 
	 * @param publicIP
	 * @param port
	 * @param username
	 * @param password
	 * @param ro
	 * @return
	 */
	public String setException(String publicIP, String port, String username, String password,
			SetException ro) {
	
		URL url;
		String cookiee = getLoginData(publicIP, port, username, password);
		JAXBElement<SetException> element = new JAXBElement<SetException>(new QName(
				"urn:com:metasolv:oms:xmlapi:1", "SetException.Request"), SetException.class, ro);
		
		StringWriter stringWirter = null;
		String message = null;
		try {
			final Marshaller marashaller = JAXBContext.newInstance(SetException.class)
					.createMarshaller();
			marashaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			stringWirter = new StringWriter();
			marashaller.marshal(element, stringWirter);
			String urlPath = urlBuilder(publicIP, port);
			message = xmlApiCall(cookiee, urlPath, stringWirter.toString());
			logger.info("Response : " + message);
		}
		catch (PropertyException e1) {
			logger.error(e1);
		}
		catch (JAXBException e) {
			logger.error(e);
		}
		logger.info("Response from OSM XML:" + message);
		return message;
	}
	
	/**
	 * 
	 * @param publicIP
	 * @param port
	 * @param username
	 * @param password
	 * @param ro
	 * @return
	 */
	public String receiveOrder(String publicIP, String port, String username, String password,
			ReceiveOrder ro) {
	
		URL url;
		String cookiee = getLoginData(publicIP, port, username, password);
		JAXBElement<ReceiveOrder> element = new JAXBElement<ReceiveOrder>(new QName(
				"urn:com:metasolv:oms:xmlapi:1", "ReceiveOrder.Request"), ReceiveOrder.class, ro);
		
		StringWriter stringWirter = null;
		String message = null;
		try {
			final Marshaller marashaller = JAXBContext.newInstance(ReceiveOrder.class)
					.createMarshaller();
			marashaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			stringWirter = new StringWriter();
			marashaller.marshal(element, stringWirter);
			String urlPath = urlBuilder(publicIP, port);
			message = xmlApiCall(cookiee, urlPath, stringWirter.toString());
			logger.info("Response : " + message);
		}
		catch (PropertyException e1) {
			logger.error(e1);
		}
		catch (JAXBException e) {
			logger.error(e);
		}
		logger.info("Response from OSM XML:" + message);
		return message;
	}
	
	/**
	 * 
	 * 
	 * @param cookiee
	 * @param urlPath
	 * @param data
	 * @return
	 */
	
	public String xmlApiCall(String cookiee, String urlPath, String data) {
	
		StringWriter StringWirter = null;
		HttpURLConnection connection = null;
		StringBuffer xmlResponseForReport = null;
		BufferedReader in = null;
		String message = null;
		URL url = null;
		if (!cookiee.equals("failure")) {
			try {
				url = new URL(urlPath);
				byte[] bytes = data.getBytes();
				logger.info("OUT:" + data);
				connection = (HttpURLConnection) url.openConnection();
				connection.setRequestMethod("POST");
				connection.setRequestProperty("Content-Type", "text/xml");
				connection.setRequestProperty("Content-Length", String.valueOf(bytes.length));
				connection.setRequestProperty("Cookie", cookiee);
				connection.setDoOutput(true);
				connection.setDoInput(true);
				BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
						connection.getOutputStream(), "UTF-8"));
				bw.write(data);
				bw.flush();
				bw.close();
				BufferedReader br = new BufferedReader(new InputStreamReader(
						connection.getInputStream()));
				StringBuilder sb = new StringBuilder();
				String line;
				while ((line = br.readLine()) != null) {
					sb.append(line + "\n");
				}
				br.close();
				connection.disconnect();
				message = sb.toString();
				logger.info("Response : " + message);
				return message;
			}
			catch (MalformedURLException e) {
				logger.error(e);
				return "{\"status\":\"invalidCredentials\"}";
			}
			catch (IOException e) {
				logger.error(e);
				return "{\"status\":\"invalidCredentials\"}";
			}
		}
		else {
			return "{\"status\":\"invalidCredentials\"}";
		}
		
	}
	
	/**
	 * 
	 * @param publicIP
	 * @param port
	 * @param username
	 * @param password
	 * @param fall
	 * @return
	 */
	public String fallOutTask(String publicIP, String port, String username, String password,
			FalloutTaskRequest fall) {
	
		URL url;
		String cookiee = getLoginData(publicIP, port, username, password);
		JAXBElement<FalloutTaskRequest> element = new JAXBElement<FalloutTaskRequest>(new QName(
				"urn:com:metasolv:oms:xmlapi:1", "FalloutTask.Request"), FalloutTaskRequest.class,
				fall);
		
		StringWriter stringWirter = null;
		String message = null;
		try {
			final Marshaller marashaller = JAXBContext.newInstance(FalloutTaskRequest.class)
					.createMarshaller();
			marashaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			stringWirter = new StringWriter();
			marashaller.marshal(element, stringWirter);
			String urlPath = urlBuilder(publicIP, port);
			message = xmlApiCall(cookiee, urlPath, stringWirter.toString());
			logger.info("Response : " + message);
		}
		catch (PropertyException e1) {
			logger.error(e1);
		}
		catch (JAXBException e) {
			logger.error(e);
		}
		logger.info("Response from OSM XML:" + message);
		return message;
	}
	
	/**
	 * 
	 * @param publicIP
	 * @param port
	 * @param username
	 * @param password
	 * @param fall
	 * @return
	 */
	public String getNextOrderAtTask(String publicIP, String port, String username,
			String password, GetNextOrderAtTask fall) {
	
		URL url;
		
		JAXBElement<GetNextOrderAtTask> element = new JAXBElement<GetNextOrderAtTask>(new QName(
				"urn:com:metasolv:oms:xmlapi:1", "GetNextOrderAtTask.Request"),
				GetNextOrderAtTask.class, fall);
		String cookiee = getLoginData(publicIP, port, username, password);
		StringWriter w = null;
		String message = null;
		try {
			final Marshaller m = JAXBContext.newInstance(GetNextOrderAtTask.class)
					.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			w = new StringWriter();
			m.marshal(element, w);
		}
		catch (PropertyException e1) {
			logger.error(e1);
		}
		catch (JAXBException e) {
			logger.error(e);
		}
		
		String urlPath = urlBuilder(publicIP, port);
		logger.info("URL:" + urlPath);
		message = xmlApiCall(cookiee, urlPath, w.toString());
		return message;
		
	}
	
	/**
	 * 
	 * @param publicIP
	 * @param port
	 * @param username
	 * @param password
	 * @param fall
	 * @return
	 */
	
	public String getOrder(String publicIP, String port, String username, String password,
			GetOrderRequest fall) {
	
		URL url;
		
		JAXBElement<GetOrderRequest> element = new JAXBElement<GetOrderRequest>(new QName(
				"urn:com:metasolv:oms:xmlapi:1", "GetOrder.Request"), GetOrderRequest.class, fall);
		String cookiee = getLoginData(publicIP, port, username, password);
		
		StringWriter w = null;
		try {
			final Marshaller m = JAXBContext.newInstance(GetOrderRequest.class).createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			w = new StringWriter();
			m.marshal(element, w);
		}
		catch (PropertyException e1) {
			logger.error(e1);
		}
		catch (JAXBException e) {
			logger.error(e);
		}
		
		HttpURLConnection connection = null;
		
		String urlPath = urlBuilder(publicIP, port);
		logger.info("URL:" + urlPath);
		String message = xmlApiCall(cookiee, urlPath, w.toString());
		return message;
		
	}
	
	/**
	 * 
	 * @param publicIP
	 * @param port
	 * @param username
	 * @param password
	 * @param fall
	 * @return
	 */
	
	public String getOrderAtTask(String publicIP, String port, String username, String password,
			GetOrderAtTaskRequest fall) {
	
		URL url;
		String cookiee = getLoginData(publicIP, port, username, password);
		JAXBElement<GetOrderAtTaskRequest> element = new JAXBElement<GetOrderAtTaskRequest>(
				new QName("urn:com:metasolv:oms:xmlapi:1", "GetOrderAtTask.Request"),
				GetOrderAtTaskRequest.class, fall);
		
		StringWriter w = null;
		String message = null;
		try {
			final Marshaller m = JAXBContext.newInstance(GetOrderAtTaskRequest.class)
					.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			w = new StringWriter();
			m.marshal(element, w);
		}
		catch (PropertyException e1) {
			logger.error(e1);
		}
		catch (JAXBException e) {
			logger.error(e);
		}
		
		String urlPath = urlBuilder(publicIP, port);
		logger.info("URL:" + urlPath);
		message = xmlApiCall(cookiee, urlPath, w.toString());
		return message;
	}
	
	/**
	 * 
	 * @param publicIP
	 * @param port
	 * @param username
	 * @param password
	 * @param fall
	 * @return
	 */
	
	public String getOrderDataHistory(String publicIP, String port, String username,
			String password, GetOrderDataHistoryRequest fall) {
	
		URL url;
		String cookiee = getLoginData(publicIP, port, username, password);
		
		JAXBElement<GetOrderDataHistoryRequest> element = new JAXBElement<GetOrderDataHistoryRequest>(
				new QName("urn:com:metasolv:oms:xmlapi:1", "GetOrderDataHistory.Request"),
				GetOrderDataHistoryRequest.class, fall);
		String message = null;
		StringWriter w = null;
		try {
			final Marshaller m = JAXBContext.newInstance(GetOrderDataHistoryRequest.class)
					.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			w = new StringWriter();
			m.marshal(element, w);
		}
		catch (PropertyException e1) {
			logger.error(e1);
		}
		catch (JAXBException e) {
			logger.error(e);
		}
		
		String urlPath = urlBuilder(publicIP, port);
		logger.info("URL:" + urlPath);
		message = xmlApiCall(cookiee, urlPath, w.toString());
		return message;
	}
	
	/**
	 * 
	 * @param publicIP
	 * @param port
	 * @param username
	 * @param password
	 * @param fall
	 * @return
	 */
	public String getOrderProcessHistory(String publicIP, String port, String username,
			String password, GetOrderProcessHistoryRequest fall) {
	
		String cookiee = getLoginData(publicIP, port, username, password);
		String message = null;
		JAXBElement<GetOrderProcessHistoryRequest> element = new JAXBElement<GetOrderProcessHistoryRequest>(
				new QName("urn:com:metasolv:oms:xmlapi:1", "GetOrderProcessHistory.Request"),
				GetOrderProcessHistoryRequest.class, fall);
		StringWriter w = null;
		try {
			final Marshaller m = JAXBContext.newInstance(GetOrderProcessHistoryRequest.class)
					.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			w = new StringWriter();
			m.marshal(element, w);
		}
		catch (PropertyException e1) {
			logger.error(e1);
		}
		catch (JAXBException e) {
			logger.error(e);
		}
		String urlPath = urlBuilder(publicIP, port);
		logger.info("URL:" + urlPath);
		message = xmlApiCall(cookiee, urlPath, w.toString());
		return message;
	}
	
	/**
	 * 
	 * @param publicIP
	 * @param port
	 * @param username
	 * @param password
	 * @param fall
	 * @return
	 */
	
	public String getOrderStateHistory(String publicIP, String port, String username,
			String password, GetOrderStateHistoryRequest fall) {
	
		JAXBElement<GetOrderStateHistoryRequest> element = new JAXBElement<GetOrderStateHistoryRequest>(
				new QName("urn:com:metasolv:oms:xmlapi:1", "GetOrderStateHistory.Request"),
				GetOrderStateHistoryRequest.class, fall);
		String cookiee = getLoginData(publicIP, port, username, password);
		
		StringWriter w = new StringWriter();
		try {
			final Marshaller m = JAXBContext.newInstance(GetOrderStateHistoryRequest.class)
					.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			m.marshal(element, w);
		}
		catch (PropertyException e1) {
			logger.error(e1);
		}
		catch (JAXBException e) {
			logger.error(e);
		}
		
		String resp = w.toString();
		byte[] bytes = resp.getBytes();
		logger.info("OUT:" + resp);
		HttpURLConnection connection = null;
		String urlPath = urlBuilder(publicIP, port);
		String message = xmlApiCall(cookiee, urlPath, w.toString());
		return message;
	}
	
	/**
	 * 
	 * @param publicIP
	 * @param port
	 * @param username
	 * @param password
	 * @param fall
	 * @return
	 */
	
	public String getTaskStatuses(String publicIP, String port, String username, String password,
			GetTaskStatusesRequest fall) {
	
		URL url;
		String cookiee = getLoginData(publicIP, port, username, password);
		
		JAXBElement<GetTaskStatusesRequest> element = new JAXBElement<GetTaskStatusesRequest>(
				new QName("urn:com:metasolv:oms:xmlapi:1", "GetTaskStatuses.Request"),
				GetTaskStatusesRequest.class, fall);
		
		StringWriter w = new StringWriter();
		try {
			final Marshaller m = JAXBContext.newInstance(GetTaskStatusesRequest.class)
					.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			m.marshal(element, w);
		}
		catch (PropertyException e1) {
			logger.error(e1);
		}
		catch (JAXBException e) {
			logger.error(e);
		}
		
		String resp = w.toString();
		byte[] bytes = resp.getBytes();
		logger.info("OUT:" + resp);
		HttpURLConnection connection = null;
		String urlPath = urlBuilder(publicIP, port);
		String message = xmlApiCall(cookiee, urlPath, w.toString());
		return message;
	}
	
	/**
	 * 
	 * @param publicIP
	 * @param port
	 * @param username
	 * @param password
	 * @param fall
	 * @return
	 */
	
	public String getUserInfo(String publicIP, String port, String username, String password,
			GetUserInfoRequest fall) {
	
		JAXBElement<GetUserInfoRequest> element = new JAXBElement<GetUserInfoRequest>(new QName(
				"urn:com:metasolv:oms:xmlapi:1", "GetUserInfo.Request"), GetUserInfoRequest.class,
				fall);
		String cookiee = getLoginData(publicIP, port, username, password);
		
		StringWriter w = new StringWriter();
		try {
			final Marshaller m = JAXBContext.newInstance(GetUserInfoRequest.class)
					.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			m.marshal(element, w);
		}
		catch (PropertyException e1) {
			logger.error(e1);
		}
		catch (JAXBException e) {
			logger.error(e);
		}
		
		String resp = w.toString();
		byte[] bytes = resp.getBytes();
		logger.info("OUT:" + resp);
		HttpURLConnection connection = null;
		String urlPath = urlBuilder(publicIP, port);
		String message = xmlApiCall(cookiee, urlPath, w.toString());
		return message;
	}
	
	/**
	 * 
	 * @param publicIP
	 * @param port
	 * @param username
	 * @param password
	 * @param fall
	 * @return
	 */
	
	public String listExceptions(String publicIP, String port, String username, String password,
			ListExceptionsRequest fall) {
	
		JAXBElement<ListExceptionsRequest> element = new JAXBElement<ListExceptionsRequest>(
				new QName("urn:com:metasolv:oms:xmlapi:1", "ListExceptions.Request"),
				ListExceptionsRequest.class, fall);
		String cookiee = getLoginData(publicIP, port, username, password);
		
		StringWriter w = new StringWriter();
		try {
			final Marshaller m = JAXBContext.newInstance(ListExceptionsRequest.class)
					.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			m.marshal(element, w);
		}
		catch (PropertyException e1) {
			logger.error(e1);
		}
		catch (JAXBException e) {
			logger.error(e);
		}
		
		String resp = w.toString();
		byte[] bytes = resp.getBytes();
		logger.info("OUT:" + resp);
		HttpURLConnection connection = null;
		String urlPath = urlBuilder(publicIP, port);
		String message = xmlApiCall(cookiee, urlPath, w.toString());
		return message;
	}
	
	/**
	 * 
	 * @param publicIP
	 * @param port
	 * @param username
	 * @param password
	 * @param fall
	 * @return
	 */
	
	public String listViews(String publicIP, String port, String username, String password,
			ListViewsRequest fall) {
	
		JAXBElement<ListViewsRequest> element = new JAXBElement<ListViewsRequest>(new QName(
				"urn:com:metasolv:oms:xmlapi:1", "ListViews.Request"), ListViewsRequest.class, fall);
		String cookiee = getLoginData(publicIP, port, username, password);
		
		StringWriter w = null;
		try {
			final Marshaller m = JAXBContext.newInstance(ListViewsRequest.class).createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			w = new StringWriter();
			m.marshal(element, w);
		}
		catch (PropertyException e1) {
			logger.error(e1);
		}
		catch (JAXBException e) {
			logger.error(e);
		}
		
		String urlPath = urlBuilder(publicIP, port);
		String message = xmlApiCall(cookiee, urlPath, w.toString());
		return message;
	}
	
	/**
	 * 
	 * @param publicIP
	 * @param port
	 * @param username
	 * @param password
	 * @param fall
	 * @return
	 */
	
	public String notifications(String publicIP, String port, String username, String password,
			NotificationsRequest fall) {
	
		JAXBElement<NotificationsRequest> element = new JAXBElement<NotificationsRequest>(
				new QName("urn:com:metasolv:oms:xmlapi:1", "Notifications.Request"),
				NotificationsRequest.class, fall);
		String cookiee = getLoginData(publicIP, port, username, password);
		
		StringWriter w = new StringWriter();
		try {
			final Marshaller m = JAXBContext.newInstance(NotificationsRequest.class)
					.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			m.marshal(element, w);
		}
		catch (PropertyException e1) {
			logger.error(e1);
		}
		catch (JAXBException e) {
			logger.error(e);
		}
		
		String resp = w.toString();
		byte[] bytes = resp.getBytes();
		logger.info("OUT:" + resp);
		HttpURLConnection connection = null;
		String urlPath = urlBuilder(publicIP, port);
		String message = xmlApiCall(cookiee, urlPath, w.toString());
		return message;
	}
	
	/**
	 * 
	 * @param publicIP
	 * @param port
	 * @param username
	 * @param password
	 * @param fall
	 * @return
	 */
	
	public String orderTypesNSources(String publicIP, String port, String username,
			String password, OrderTypesNSourcesRequest fall) {
	
		JAXBElement<OrderTypesNSourcesRequest> element = new JAXBElement<OrderTypesNSourcesRequest>(
				new QName("urn:com:metasolv:oms:xmlapi:1", "OrderTypesNSources.Request"),
				OrderTypesNSourcesRequest.class, fall);
		String cookiee = getLoginData(publicIP, port, username, password);
		
		StringWriter w = new StringWriter();
		try {
			final Marshaller m = JAXBContext.newInstance(OrderTypesNSourcesRequest.class)
					.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			m.marshal(element, w);
		}
		catch (JAXBException e1) {
			logger.error(e1);
		}
		
		String urlPath = urlBuilder(publicIP, port);
		String message = xmlApiCall(cookiee, urlPath, w.toString());
		return message;
	}
	
	/**
	 * 
	 * @param publicIP
	 * @param port
	 * @param username
	 * @param password
	 * @param remark
	 * @return
	 */
	
	public String modifyRemark(String publicIP, String port, String username, String password,
			ModifyRemark remark) {
	
		StringWriter w = xmlString(remark);
		String cookiee = getLoginData(publicIP, port, username, password);
		
		String urlPath = urlBuilder(publicIP, port);
		logger.info("URL:" + urlPath);
		String message = xmlApiCall(cookiee, urlPath, w.toString());
		
		return message;
	}
	
	private StringWriter xmlString(ModifyRemark remark) {
	
		JAXBElement<ModifyRemark> element = new JAXBElement<ModifyRemark>(new QName(
				"urn:com:metasolv:oms:xmlapi:1", "ModifyRemark.Request"), ModifyRemark.class,
				remark);
		
		StringWriter w = new StringWriter();
		try {
			final Marshaller m = JAXBContext.newInstance(ModifyRemark.class).createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			m.marshal(element, w);
		}
		catch (JAXBException e1) {
			logger.error(e1);
		}
		return w;
	}
	
	/**
	 * 
	 * @param publicIP
	 * @param port
	 * @param username
	 * @param password
	 * @param query
	 * @return
	 */
	
	public String queryRequest(String publicIP, String port, String username, String password,
			String query) {
	
		String cookiee = getLoginData(publicIP, port, username, password);
		StringWriter w = new StringWriter();
		
		String urlPath = urlBuilder(publicIP, port);
		String message = xmlApiCall(cookiee, urlPath, query);
		return message;
	}
	
	/**
	 * 
	 * @param publicIP
	 * @param port
	 * @param username
	 * @param password
	 * @param query
	 * @return
	 */
	
	public String copyOrder(String publicIP, String port, String username, String password,
			CopyOrder copyOrder) {
	
		String cookiee = getLoginData(publicIP, port, username, password);
		StringWriter w = new StringWriter();
		JAXBElement<CopyOrder> element = new JAXBElement<CopyOrder>(new QName(
				"urn:com:metasolv:oms:xmlapi:1", "CopyOrder.Request"), CopyOrder.class, copyOrder);
		
		try {
			final Marshaller m = JAXBContext.newInstance(CopyOrder.class).createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			m.marshal(element, w);
		}
		catch (JAXBException e1) {
			logger.error(e1);
		}
		String urlPath = urlBuilder(publicIP, port);
		String message = xmlApiCall(cookiee, urlPath, w.toString());
		return message;
	}
	
	/**
	 * 
	 * @param publicIP
	 * @param port
	 * @param username
	 * @param password
	 * @param query
	 * @return
	 */
	
	public String updateOrder(String publicIP, String port, String username, String password,
			String updateOrder) {
	
		String cookiee = getLoginData(publicIP, port, username, password);
		String urlPath = urlBuilder(publicIP, port);
		String message = xmlApiCall(cookiee, urlPath, updateOrder);
		return message;
	}
	
	public String addRemark(String publicIP, String port, String username, String password,
			String remark) {
	
		String cookiee = getLoginData(publicIP, port, username, password);
		String urlPath = urlBuilder(publicIP, port);
		String message = xmlApiCall(cookiee, urlPath, remark);
		return message;
	}
	
	private String urlBuilder(String publicIP, String port) {
	
		return (new StringBuilder("http://")).append(publicIP).append(":").append(port)
				.append("/OrderManagement/XMLAPI/XMLAPI").toString();
	}
}
