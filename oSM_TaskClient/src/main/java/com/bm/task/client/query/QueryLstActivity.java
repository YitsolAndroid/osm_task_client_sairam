/*
 * Copyright (C) 2015, 2016, Blue Matrix and/or its affiliates. All rights reserved. 
 */
package com.bm.task.client.query;

import java.util.ArrayList;
import java.util.List;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.Spinner;

import com.bm.task.client.restWS.GetQueryData;
import com.bm.task.client.utils.FontType;
import com.bm.task.client.xml.WorkListDomain;
import com.example.osm_xmlapi.R;

public class QueryLstActivity extends Fragment {

	public static String selected;

	private ExpandableListView workList;
	private int lastExpandedPosition = -1;

	private List<WorkListDomain> lst = new ArrayList<WorkListDomain>();
	private ArrayList<ArrayList<WorkListDomain>> childList = new ArrayList<ArrayList<WorkListDomain>>();
	private WorkListDomain workListDomain;

	private Spinner spnSelect;

	private String[] spinnerArray = { "Editor", "Add Remark", "Process History" };

	private String xml;

	protected Context context;

	private ViewGroup root;

	public QueryLstActivity(Context con,String xml) {
		this.context = con;
		this.xml = xml;
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.querylist, container, false);
		
		root = (ViewGroup)rootView.findViewById(R.id.ll_queryListactivity);
		new FontType(context, root);

		workList = (ExpandableListView) rootView
				.findViewById(R.id.lst_workList);


		spnSelect = (Spinner) rootView.findViewById(R.id.spn_select);

		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);

		System.out.println("**********xml********" + xml);

		GetQueryData getQueryData = new GetQueryData(xml);
		lst = getQueryData.getQueryList();
		System.out.println("**********ListSize********" + lst.size());

		spnSelect.setAdapter(new SpnAdapter(context, spinnerArray));

		spnSelect.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int pos, long id) {

				selected = SpnAdapter.lst[pos];
				System.out.println(selected);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});

		for (int i = 0; i < lst.size(); i++) {
			ArrayList<WorkListDomain> mylst = new ArrayList<WorkListDomain>();
			workListDomain = new WorkListDomain();
			workListDomain.setSuspend("Suspend Order");
			workListDomain.setResume("Resume Order");
			workListDomain.setAbort("Abort Order");
			workListDomain.setCancel("Cancel Order");
			mylst.add(workListDomain);
			childList.add(mylst);
		}

		workList.setAdapter(new QueryExpndListAdaptr(context, lst, childList,
				workList));

		workList.setOnGroupExpandListener(new OnGroupExpandListener() {

			public void onGroupExpand(int groupPosition) {

				if (lastExpandedPosition != -1
						&& groupPosition != lastExpandedPosition) {
					workList.collapseGroup(lastExpandedPosition);
				}
				lastExpandedPosition = groupPosition;
			}
		});

		
		
		return rootView;

	}

	public void onBackPressed() {

	}

}
