
package com.blue.matrix.task.client.model;

/**
 * 
 * @author jaya sankar
 *
 */

public class AllOrderStates {
	
	private String stateId;
	private String statedesc;
	
	public String getStateId() {
	
		return stateId;
	}
	
	public void setStateId(String stateId) {
	
		this.stateId = stateId;
	}
	
	public String getStatedesc() {
	
		return statedesc;
	}
	
	public void setStatedesc(String statedesc) {
	
		this.statedesc = statedesc;
	}
	
}
