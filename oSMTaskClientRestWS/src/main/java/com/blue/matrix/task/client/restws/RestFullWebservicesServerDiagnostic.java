
package com.blue.matrix.task.client.restws;

/**
 * 
 * @author Sahil Nokhwal
 * 
 * @FileName: RestFullWebservicesServerDiagnostic.java
 * 
 * @version: 1.0.0.0.0
 * 
 * @Descrption This class is use for accessing Restful
 *             Webservices
 *
 */

import java.io.InputStream;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.xml.soap.SOAPException;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import com.blue.matrix.task.client.WebserviceHTTPClient;
import com.blue.matrix.task.client.WebserviceHTTPClientDiagnostic;
import com.blue.matrix.task.client.util.RestfulJsonUtil;

@Path("/orderServices")
public class RestFullWebservicesServerDiagnostic {
	
	@Context
	ServletContext ctx;
	@Context
	ContainerRequestContext request;
	private JSONObject jsonOutputObject;
	private String username;
	private String password;
	private String orderId;
	public WebserviceHTTPClientDiagnostic wsHttpClientDiag = null;
	private JSONObject jsonInputObject;
	final static Logger logger = Logger.getLogger(RestFullWebservicesServer.class);
	
	{
		try {
			
			wsHttpClientDiag = new WebserviceHTTPClientDiagnostic();
		}
		catch (Exception e) {
			logger.error(e);
		}
	}
	
	// setter method for Username, Password, OrderId
	/**
	 * 
	 * @param jsonInputObject
	 * @throws JSONException
	 */
	private void setter(JSONObject jsonInputObject) throws JSONException {
	
		username = (String) request.getProperty("username"); //$NON-NLS-1$
		password = (String) request.getProperty("password"); //$NON-NLS-1$
		orderId = jsonInputObject.getString("orderId"); //$NON-NLS-1$
		
	}
	
	/**
	 * 
	 * @param incommingJsonObj
	 * @param ctx
	 * @return
	 */
	// getOrderCompensationplan Restful WebService
	@POST
	@Path("/getOrderCompensationplan")
	@Produces("application/json")
	@Consumes("application/json")
	public String getOrderCompensationplan(InputStream incommingJsonObj) {
	
		final String soapMessageAsString;
		String responsedata = null;
		
		try {
			jsonInputObject = new JSONObject(
					RestfulJsonUtil.convertInputDataToJsonString(incommingJsonObj));
			// here we are getting response from soap webservices
			setter(jsonInputObject);
			
			soapMessageAsString = wsHttpClientDiag.getOrderCompensationplan(username, password,
					orderId, ctx);
			
			responsedata = RestfulJsonUtil.convertSoapMessageToJsonString(soapMessageAsString);
		}
		catch (JSONException e) {
			logger.error(e);
			return Messages.getString("ExceptionMsg"); //$NON-NLS-1$
		}
		catch (SOAPException e) {
			
			logger.error(e);
			return Messages.getString("ExceptionMsg4"); //$NON-NLS-1$
		}
		catch (Exception e) {
			logger.error(e);
			return Messages.getString("ExceptionMsg5"); //$NON-NLS-1$
		}
		
		return responsedata;
	}
	
	/**
	 * 
	 * @param incommingJsonObj
	 * @param ctx
	 * @return
	 */
	// getCompensationplan Restful WebService
	@POST
	@Path("/getCompensationplan")
	@Produces("application/json")
	@Consumes("application/json")
	public String getCompensationplan(InputStream incommingJsonObj) {
	
		final String soapMessageAsString;
		String responsedata = null;
		
		try {
			jsonInputObject = new JSONObject(
					RestfulJsonUtil.convertInputDataToJsonString(incommingJsonObj));
			// here we are getting response from soap webservices
			setter(jsonInputObject);
			
			soapMessageAsString = wsHttpClientDiag.getCompensationplan(username, password, orderId,
					ctx);
			
			responsedata = RestfulJsonUtil.convertSoapMessageToJsonString(soapMessageAsString);
		}
		catch (JSONException e) {
			logger.error(e);
			return Messages.getString("ExceptionMsg6"); //$NON-NLS-1$
		}
		catch (SOAPException e) {
			
			logger.error(e);
			return Messages.getString("ExceptionMsg7"); //$NON-NLS-1$
		}
		catch (Exception e) {
			logger.error(e);
			return Messages.getString("ExceptionMsg8"); //$NON-NLS-1$
		}
		
		return responsedata;
	}
	
	/**
	 * 
	 * @param incommingJsonObj
	 * @param ctx
	 * @return
	 */
	// getOrderProcessHistory Restful WebService
	@POST
	@Path("/getOrderProcessHistory")
	@Produces("application/json")
	@Consumes("application/json")
	public String getOrderProcessHistory(InputStream incommingJsonObj) {
	
		final String soapMessageAsString;
		String responsedata = null;
		
		try {
			jsonInputObject = new JSONObject(
					RestfulJsonUtil.convertInputDataToJsonString(incommingJsonObj));
			// here we are getting response from soap webservices
			setter(jsonInputObject);
			String comp = jsonInputObject.getString("compensationId"); //$NON-NLS-1$
			soapMessageAsString = wsHttpClientDiag.getOrderProcessHistory(username, password,
					orderId, comp, ctx);
			
			responsedata = RestfulJsonUtil.convertSoapMessageToJsonString(soapMessageAsString);
		}
		catch (JSONException e) {
			logger.error(e);
			return Messages.getString("ExceptionMsg10"); //$NON-NLS-1$
		}
		catch (SOAPException e) {
			
			logger.error(e);
			return Messages.getString("ExceptionMsg11"); //$NON-NLS-1$
		}
		catch (Exception e) {
			logger.error(e);
			return Messages.getString("ExceptionMsg12"); //$NON-NLS-1$
		}
		
		return responsedata;
	}
	
}
