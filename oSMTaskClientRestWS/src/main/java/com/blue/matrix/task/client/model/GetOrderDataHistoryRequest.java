
package com.blue.matrix.task.client.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author jaya sankar
 *
 */

@XmlRootElement
public class GetOrderDataHistoryRequest {
	
	@XmlElement(name = "OrderID")
	private String orderId;
	
	@XmlElement(name = "ViewID")
	private String viewId;
	
	public void setOrderId(String orderId) {
	
		this.orderId = orderId;
	}
	
	public void setViewId(String viewId) {
	
		this.viewId = viewId;
	}
}
