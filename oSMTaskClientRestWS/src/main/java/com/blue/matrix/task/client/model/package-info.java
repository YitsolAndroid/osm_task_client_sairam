/**
 * @author jaya sankar
 *
 */

@javax.xml.bind.annotation.XmlSchema(namespace = "urn:com:metasolv:oms:xmlapi:1", elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED, xmlns = { @javax.xml.bind.annotation.XmlNs(namespaceURI = "urn:com:metasolv:oms:xmlapi:1", prefix = "") })
package com.blue.matrix.task.client.model;