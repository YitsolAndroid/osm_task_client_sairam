
package com.blue.matrix.task.client.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author jaya sankar
 *
 */

@XmlRootElement
public class DeleteAttachmnets {
	
	@XmlElement(name = "AttachmentID")
	private String attachmentId;
	
	public String getAttachmentId() {
	
		return attachmentId;
	}
	
	public void setAttachmentId(String attachmentId) {
	
		this.attachmentId = attachmentId;
	}
}
