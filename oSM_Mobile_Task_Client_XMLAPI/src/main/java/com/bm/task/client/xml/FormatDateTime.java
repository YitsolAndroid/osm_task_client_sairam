/*
 * Copyright (C) 2015, 2016, Blue Matrix and/or its affiliates. All rights reserved.
 */
package com.bm.task.client.xml;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * To get Custom Date time
 *
 * @author Bhavya
 */
public class FormatDateTime {

    private int days;
    private int hrs;
    private int mins;
    private int secs;

    /**
     * To getcustom Date and Time
     *
     * @param formateDate
     * @return
     * @throws ParseException
     */

    public String getFormattedDateTime(String formateDate)
            throws ParseException {

        System.out.println(formateDate);

        String[] dateList = formateDate.split("T");
        String date = null;
        if (dateList.length == 2) {
            date = dateList[0] + " " + dateList[1];
            dateList = date.split("IS");
            date = dateList[0];
        }

        try {
            DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            DateFormat outputFormat = new SimpleDateFormat(
                    "dd/MM/yyyy\tKK:mm:ss a");
            if (date != null)
                date = outputFormat.format(inputFormat.parse(date));
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return date;
    }

    /**
     * convert seconds to days hours mins and secs
     *
     * @param actualDuration
     * @return
     */

    public String getSecondConverter(int actualDuration) {
        days = actualDuration / 86400;
        actualDuration = actualDuration % 86400;
        hrs = actualDuration / 3600;
        actualDuration = actualDuration % 3600;
        mins = actualDuration / 60;
        actualDuration = actualDuration % 60;
        secs = actualDuration;

        String duration = "ActualDuration : " + days + "day(s) " + hrs + "hr(s) " + mins + "min(s) " + secs + "sec(s)";

        return duration;


    }


}
