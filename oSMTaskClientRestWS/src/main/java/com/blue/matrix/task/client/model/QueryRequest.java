
package com.blue.matrix.task.client.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author jaya sankar
 *
 */

@XmlRootElement
public class QueryRequest {
	
	@XmlElement(name = "OrderID")
	private String orderId;
	
	@XmlElement(name = "Reference")
	private String reference;
	
	@XmlElement(name = "OrderState")
	private String orderState;
	
	@XmlElement(name = "State")
	private String state;
	
	@XmlElement(name = "Status")
	private String status;
	
	@XmlElement(name = "User")
	private String user;
	
	@XmlElement(name = "Namespace")
	private String nameSpace;
	
	@XmlElement(name = "OrderType")
	private String orderType;
	
	@XmlElement(name = "OrderSource")
	private String orderSource;
	
	@XmlElement(name = "ExecutionMode")
	private String exceutionMode;
	
	@XmlElement(name = "Task")
	private String task;
	
	public String getOrderId() {
	
		return orderId;
	}
	
	public void setOrderId(String orderId) {
	
		this.orderId = orderId;
	}
	
	public String getReference() {
	
		return reference;
	}
	
	public void setReference(String reference) {
	
		this.reference = reference;
	}
	
	public String getOrderState() {
	
		return orderState;
	}
	
	public void setOrderState(String orderState) {
	
		this.orderState = orderState;
	}
	
	public String getState() {
	
		return state;
	}
	
	public void setState(String state) {
	
		this.state = state;
	}
	
	public String getStatus() {
	
		return status;
	}
	
	public void setStatus(String status) {
	
		this.status = status;
	}
	
	public String getUser() {
	
		return user;
	}
	
	public void setUser(String user) {
	
		this.user = user;
	}
	
	public String getNameSpace() {
	
		return nameSpace;
	}
	
	public void setNameSpace(String nameSpace) {
	
		this.nameSpace = nameSpace;
	}
	
	public String getOrderType() {
	
		return orderType;
	}
	
	public void setOrderType(String orderType) {
	
		this.orderType = orderType;
	}
	
	public String getOrderSource() {
	
		return orderSource;
	}
	
	public void setOrderSource(String orderSource) {
	
		this.orderSource = orderSource;
	}
	
	public String getExceutionMode() {
	
		return exceutionMode;
	}
	
	public void setExceutionMode(String exceutionMode) {
	
		this.exceutionMode = exceutionMode;
	}
	
	public String getTask() {
	
		return task;
	}
	
	public void setTask(String task) {
	
		this.task = task;
	}
	
}
