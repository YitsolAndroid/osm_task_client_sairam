
package com.blue.matrix.task.client.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author jaya sankar
 *
 */

@XmlRootElement
public class GetOrderProcessHistoryRequest {
	
	@XmlElement(name = "OrderID")
	private String orderId;
	
	public void setOrderId(String orderId) {
	
		this.orderId = orderId;
	}
}
