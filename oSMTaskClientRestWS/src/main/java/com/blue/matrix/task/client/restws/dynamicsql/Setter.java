
package com.blue.matrix.task.client.restws.dynamicsql;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.blue.matrix.task.client.DB_Connection;
import com.blue.matrix.task.client.dynamicsql.SearchCriteria;

// Referenced classes of package com.blue.matrix.task.client.restws.dynamicsql:
//            TableData, JoinListData, RJoinListData, GroupListData, 
//            CriteriaListData

public class Setter {
	
	private DB_Connection dbConn;
	private SearchCriteria searchCriteria;
	private List tableList;
	private List joinList;
	private List RJoinList;
	private List groupList;
	private List criteriaList;
	private JSONArray jsonTables;
	private JSONArray jsonJoinList;
	private JSONArray jsonRJoinList;
	private JSONArray jsongroupList;
	private JSONArray jsonCriteriaList;
	
	public Setter ( ) {
	
		jsonTables = null;
		jsonJoinList = null;
		jsonRJoinList = null;
		jsongroupList = null;
		jsonCriteriaList = null;
	}
	
	void setter(JSONObject jsonInputObject) {
	
		try {
			jsonTables = jsonInputObject.getJSONArray("tables");
			TableData td = new TableData();
			tableList = td.setTable(jsonTables);
			jsonJoinList = jsonInputObject.getJSONArray("joinList");
			JoinListData jld = new JoinListData();
			joinList = jld.setJoinList(jsonJoinList);
			jsonRJoinList = jsonInputObject.getJSONArray("rJoinList");
			RJoinListData rjld = new RJoinListData();
			RJoinList = rjld.setRJoinList(jsonRJoinList);
			jsongroupList = jsonInputObject.getJSONArray("groupList");
			GroupListData gld = new GroupListData();
			groupList = gld.setGroupList(jsongroupList);
			jsonCriteriaList = jsonInputObject.getJSONArray("criteriaList");
			CriteriaListData cld = new CriteriaListData();
			criteriaList = cld.setCriteriaList(jsonCriteriaList);
		}
		catch (JSONException e) {
			
			e.printStackTrace();
		}
		catch (Exception e) {
			
			e.printStackTrace();
		}
	}
	
	ArrayList call() {
	
		ArrayList outputArray = null;
		dbConn = new DB_Connection();
		// outputArray = (ArrayList)
		dbConn.build(tableList, joinList, RJoinList, groupList, criteriaList); // .getVaraible();
		return outputArray;
	}
}
