
package com.blue.matrix.task.client.restws.dynamicsql;

import com.blue.matrix.task.client.dynamicsql.SearchCriteria;
import com.blue.matrix.task.client.dynamicsql.Table;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import org.json.*;

public class CriteriaListData {
	
	private List criteriaList;
	private JSONArray jsonTables;
	private List tableList;
	private JSONArray jsonCriteriaList;
	
	public CriteriaListData ( ) {
	
		jsonTables = null;
		jsonCriteriaList = null;
	}
	
	List setCriteriaList(JSONArray jsonCriteriaList) throws JSONException {
	
		if (jsonCriteriaList != null) {
			try {
				criteriaList = new ArrayList();
				for (int i = 0; i < jsonCriteriaList.length(); i++) {
					JSONObject jo = new JSONObject(jsonCriteriaList.get(i).toString());
					SearchCriteria sc = new SearchCriteria();
					String value = jo.get("value").toString();
					String opertor = jo.get("opertor").toString();
					String table1Colum1 = jo.get("table1Colum1").toString();
					Object obj = jo.get("values");
					sc.setValue(value);
					sc.setOpertor(opertor);
					sc.setTable1Colum1(table1Colum1);
					sc.setValues(obj);
					
					JSONObject jt = new JSONObject(jo.get("tablec1").toString());
					Table tb = new Table(jt.getString("name"), jt.getString("alias"));
					if (jt.getJSONArray("columnsWithAlias") != null) {
						JSONArray jArrayColumn = jt.getJSONArray("columnsWithAlias");
						
						for (int j = 0; j < jArrayColumn.length(); j++) {
							List sCol = null;
							sCol = new ArrayList();
							sCol.add(jArrayColumn.getString(j));
							
							tb.addColumnsToSelect(sCol);
							
						}
						
					}
					sc.setTable1(tb);
					criteriaList.add(sc);
				}
				
			}
			catch (JSONException e) {
				
			}
			catch (Exception e) {
				
				e.printStackTrace();
			}
		}
		return criteriaList;
	}
}
