package com.bm.task.client.xml;

public class RemarkDomain {
	
	private String text	;
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getState() {
		return State;
	}
	public void setState(String state) {
		State = state;
	}
	public String getUserName() {
		return UserName;
	}
	public void setUserName(String userName) {
		UserName = userName;
	}
	public String getTaskName() {
		return TaskName;
	}
	public void setTaskName(String taskName) {
		TaskName = taskName;
	}
	public String getTimeStamp() {
		return TimeStamp;
	}
	public void setTimeStamp(String timeStamp) {
		TimeStamp = timeStamp;
	}
	public String getId() {
		return Id;
	}
	public void setId(String id) {
		Id = id;
	}
	private String State ;
	private String UserName ;
	private String TaskName;
	private String TimeStamp;
	private String Id;

}
