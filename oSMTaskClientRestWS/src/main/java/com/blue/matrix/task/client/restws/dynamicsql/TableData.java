
package com.blue.matrix.task.client.restws.dynamicsql;

import com.blue.matrix.task.client.dynamicsql.Table;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import org.json.*;

public class TableData {
	
	private List tableList;
	private JSONArray jsonTables;
	
	public TableData ( ) {
	
		jsonTables = null;
	}
	
	List setTable(JSONArray jsonTables) {
	
		if (jsonTables != null) {
			try {
				tableList = new ArrayList();
				for (int i = 0; i < jsonTables.length(); i++) {
					JSONObject jo = new JSONObject(jsonTables.get(i).toString());
					Table tb = new Table(jo.getString("name"), jo.getString("alias"));
					if (jo.getJSONArray("columnsWithAlias") != null) {
						JSONArray jArrayColumn = jo.getJSONArray("columnsWithAlias");
						for (int j = 0; j < jArrayColumn.length(); j++) {
							List sCol = null;
							sCol = new ArrayList();
							sCol.add(jArrayColumn.getString(j));
							tb.addColumnsToSelect(sCol);
						}
						
					}
					tableList.add(tb);
					
				}
				
			}
			catch (JSONException e) {
				
				e.printStackTrace();
			}
			catch (Exception e) {
				
				e.printStackTrace();
			}
		}
		return tableList;
	}
}
