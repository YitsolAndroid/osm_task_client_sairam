
package com.blue.matrix.task.client;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.StringWriter;
import java.net.URL;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFault;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;

/**
 * 
 * @author Jaya sankar
 *
 */
public class WebserviceHTTPClientDiagnostic {
	
	final static Logger logger = Logger.getLogger(WebserviceHTTPClientDiagnostic.class);
	
	private String URL;
	private String GET_ORDER_COMPENSATIONS;
	private String GET_ORDER_PROCESS_HISTORY;
	private String GET_COMPENSATIONS_PLAN;
	private static MessageFactory messageFactory;
	private static SOAPConnectionFactory factory;
	private static SOAPConnection connection;
	private static URL endpoint;
	
	final private static String ws = "http://xmlns.oracle.com/communications/ordermanagement";
	final private static String wsse = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
	final private static String wsu = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd";
	final private static String passwordType = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText";
	final private static String soapenv = "http://schemas.xmlsoap.org/soap/envelope/";
	
	public WebserviceHTTPClientDiagnostic ( ) throws Exception {
	
		Properties props = new Properties();
		props.load(Thread.currentThread().getContextClassLoader()
				.getResourceAsStream("/WebserviceClient.properties"));
		URL = props.getProperty("weblogic.webservice.diagnostic.uri");
		GET_ORDER_COMPENSATIONS = props.getProperty("getOrderCompensationplan");
		GET_ORDER_PROCESS_HISTORY = props.getProperty("getOrderProcessHistory");
		GET_COMPENSATIONS_PLAN = props.getProperty("getCompensationplan");
		messageFactory = MessageFactory.newInstance();
		System.setProperty("javax.xml.soap.MessageFactory",
				"com.sun.xml.internal.messaging.saaj.soap.ver1_1.SOAPMessageFactory1_1Impl");
		System.setProperty("javax.xml.soap.SOAPConnectionFactory",
				"weblogic.wsee.saaj.SOAPConnectionFactoryImpl");
		factory = SOAPConnectionFactory.newInstance();
		connection = factory.createConnection();
		endpoint = new URL(URL);
	}
	
	/**
	 * 
	 * @param message
	 * @throws Exception
	 */
	private void addWSSecurityHeader(SOAPMessage message, String username, String password)
			throws Exception {
	
		String temp = username + ":" + password;
		String authorization = new String(Base64.encodeBase64(temp.getBytes()));
		MimeHeaders hd = message.getMimeHeaders();
		hd.addHeader("Authorization", "Basic " + authorization);
		
		SOAPEnvelope envelope = message.getSOAPPart().getEnvelope();
		SOAPHeader header = message.getSOAPHeader();
		Name name = envelope.createName("Security", "wsse", wsse);
		SOAPElement security = header.addChildElement(name);
		name = envelope.createName("mustUnderstand", "soapenv", soapenv);
		security.addAttribute(name, "1");
		
		name = envelope.createName("UsernameToken", "wsse", wsse);
		SOAPElement usernameToken = security.addChildElement(name);
		name = envelope.createName("Id", "wsu", wsu);
		usernameToken.addAttribute(name, "UsernameToken-4799946");
		
		name = envelope.createName("Username", "wsse", wsse);
		SOAPElement usernameElement = usernameToken.addChildElement(name);
		usernameElement.setTextContent(username);
		
		name = envelope.createName("Password", "wsse", wsse);
		SOAPElement passwordElement = usernameToken.addChildElement(name);
		name = envelope.createName("Type");
		passwordElement.addAttribute(name, passwordType);
		passwordElement.setTextContent(password);
		message.saveChanges();
	}
	
	/**
	 * This Method will create a soap call to Osm Server to Get
	 * OrderCompensationplan
	 * 
	 * @param username
	 * @param password
	 * @param orderId
	 * @param ctx
	 * @return
	 * @throws Exception
	 */
	public String getOrderCompensationplan(String username, String password, String orderId,
			ServletContext ctx) throws Exception {
	
		SOAPMessage message = messageFactory.createMessage();
		message.getMimeHeaders().setHeader("SOAPAction", ws + "/Orders");
		
		File file = new File(ctx.getResource(GET_ORDER_COMPENSATIONS).getPath());
		addingCompensation(username, password, orderId, message, file);
		String soapmessage = soapCall(message, connection, endpoint);
		
		logger.info("Soap response" + soapmessage);
		return soapmessage;
		
	}
	
	/**
	 * @param username
	 * @param password
	 * @param orderId
	 * @param message
	 * @param file
	 * @throws FileNotFoundException
	 * @throws Exception
	 * @throws SOAPException
	 * @throws DOMException
	 */
	private void addingCompensation(String username, String password, String orderId,
			SOAPMessage message, File file) throws FileNotFoundException, Exception, SOAPException,
			DOMException {
	
		FileInputStream fis = new FileInputStream(file);
		
		setMessageContent(message, fis);
		
		SOAPElement orderIdElement = (SOAPElement) message.getSOAPBody()
				.getElementsByTagName("OrderId").item(0);
		orderIdElement.setTextContent(orderId);
		message.saveChanges();
		
		addWSSecurityHeader(message, username, password);
	}
	
	/**
	 * This Method will create a soap call to Osm Server to Get Compensationplan
	 * 
	 * @param username
	 * @param password
	 * @param orderId
	 * @param ctx
	 * @return
	 * @throws Exception
	 */
	public String getCompensationplan(String username, String password, String orderId,
			ServletContext ctx) throws Exception {
	
		SOAPMessage message = messageFactory.createMessage();
		message.getMimeHeaders().setHeader("SOAPAction", ws + "/Orders");
		
		File file = new File(ctx.getResource(GET_COMPENSATIONS_PLAN).getPath());
		addingCompensation(username, password, orderId, message, file);
		String soapmessage = soapCall(message, connection, endpoint);
		return soapmessage;
		
	}
	
	/**
	 * This Method will create a soap call to Osm Server to Get
	 * OrderProcessHistory
	 * 
	 * @param username
	 * @param password
	 * @param orderId
	 * @param compensationId
	 * @param perspective
	 * @param ctx
	 * @return
	 * @throws Exception
	 */
	
	public String getOrderProcessHistory(String username, String password, String orderId,
			String compensation, ServletContext ctx) throws Exception {
	
		SOAPMessage message = messageFactory.createMessage();
		message.getMimeHeaders().setHeader("SOAPAction", ws + "/Orders");
		File file = new File(ctx.getResource(GET_ORDER_PROCESS_HISTORY).getPath());
		FileInputStream fis = new FileInputStream(file);
		setMessageContent(message, fis);
		
		SOAPElement orderIdElement = (SOAPElement) message.getSOAPBody()
				.getElementsByTagName("OrderId").item(0);
		orderIdElement.setTextContent(orderId);
		
		SOAPElement CompensationIdElement = (SOAPElement) message.getSOAPBody()
				.getElementsByTagName("CompensationId").item(0);
		CompensationIdElement.setTextContent(compensation);
		
		String soapmessage = soapInitializer(username, password, message);
		return soapmessage;
		
	}
	
	/**
	 * @param username
	 * @param password
	 * @param message
	 * @return
	 * @throws SOAPException
	 * @throws Exception
	 */
	private String soapInitializer(String username, String password, SOAPMessage message)
			throws SOAPException, Exception {
	
		message.saveChanges();
		
		addWSSecurityHeader(message, username, password);
		String soapmessage = soapCall(message, connection, endpoint);
		
		logger.info("Soap response" + soapmessage);
		return soapmessage;
	}
	
	/**
	 * 
	 * @param message
	 * @param file
	 * @throws Exception
	 */
	public static void setMessageContent(SOAPMessage message, FileInputStream file)
			throws Exception {
	
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		dbFactory.setNamespaceAware(true);
		
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document document = dBuilder.parse(file);
		DOMSource domSource = new DOMSource(document);
		
		message.getSOAPPart().setContent(domSource);
		
	}
	
	/**
	 * 
	 * @param message
	 * @param connection
	 * @param endpoint
	 * @return
	 * @throws Exception
	 */
	public String soapCall(SOAPMessage message, SOAPConnection connection, URL endpoint)
			throws Exception {
	
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		message.writeTo(out);
		SOAPMessage response = connection.call(message, endpoint);
		SOAPBody body = response.getSOAPBody();
		SOAPFault fault = body.getFault();
		
		if (fault != null) {
			logger.info(fault.getFaultString());
		}
		ByteArrayOutputStream resp = new ByteArrayOutputStream();
		response.writeTo(resp);
		
		DOMSource source = new DOMSource(body);
		StringWriter stringResult = new StringWriter();
		TransformerFactory.newInstance().newTransformer()
				.transform(source, new StreamResult(stringResult));
		
		String soapmessage = stringResult.toString();
		
		return soapmessage;
		
	}
	
}
