
package com.blue.matrix.task.client.exception;

import java.sql.SQLException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.log4j.Logger;

import com.blue.matrix.task.client.DB_Connection;

/**
 * 
 * @author jaya sankar
 *
 */

@Provider
public class GenericException implements ExceptionMapper<GenericAllException> {
	
	final static Logger logger = Logger.getLogger(GenericException.class);
	
	@Override
	public Response toResponse(GenericAllException arg0) {
	
		logger.info("-------------GenericAllException Occured--------------");
		return Response.status(Response.Status.SERVICE_UNAVAILABLE)
				.entity("{\"status\":401,\"errorMsg\":\"DataBase Connection  not found!\"}")
				.type(MediaType.APPLICATION_JSON).build();
	}
	
}
