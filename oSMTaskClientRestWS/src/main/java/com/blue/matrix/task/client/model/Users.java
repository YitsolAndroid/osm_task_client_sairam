
package com.blue.matrix.task.client.model;

/**
 * 
 * @author jaya sankar
 *
 */

public class Users {
	
	private String userName;
	private String workGroup;
	
	public String getUserName() {
	
		return userName;
	}
	
	public void setUserName(String userName) {
	
		this.userName = userName;
	}
	
	public String getWorkGroup() {
	
		return workGroup;
	}
	
	public void setWorkGroup(String workGroup) {
	
		this.workGroup = workGroup;
	}
	
}
