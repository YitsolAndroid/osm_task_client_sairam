
package com.blue.matrix.task.client.restws.dynamicsql;

import com.blue.matrix.task.client.dynamicsql.Join;
import com.blue.matrix.task.client.dynamicsql.Table;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import org.json.*;

public class JoinListData {
	
	private List joinList;
	
	public JoinListData ( ) {
	
	}
	
	List setJoinList(JSONArray jsonJoinList) {
	
		if (jsonJoinList != null) {
			try {
				joinList = new ArrayList();
				for (int i = 0; i < jsonJoinList.length(); i++) {
					JSONObject jo = new JSONObject(jsonJoinList.get(i).toString());
					JSONObject jt1 = (JSONObject) jo.get("table1");
					String name1 = jt1.getString("name");
					String alias1 = jt1.getString("alias");
					Table tb1 = new Table(name1, alias1);
					JSONObject jt2 = (JSONObject) jo.get("table2");
					String name2 = jt2.getString("name");
					String alias2 = jt2.getString("alias");
					Table tb2 = new Table(name2, alias2);
					Join join = new Join();
					join.setTable1Colum1(jo.get("table1Colum1").toString());
					join.setTable2Colum2(jo.get("table2Colum2").toString());
					join.setTable1(tb1);
					join.setTable2(tb2);
					joinList.add(join);
				}
				
			}
			catch (JSONException e) {
				
				e.printStackTrace();
			}
			catch (Exception e) {
				
				e.printStackTrace();
			}
		}
		return joinList;
	}
}
