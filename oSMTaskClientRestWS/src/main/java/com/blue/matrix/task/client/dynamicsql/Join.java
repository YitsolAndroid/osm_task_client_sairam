
package com.blue.matrix.task.client.dynamicsql;

/**
 * 
 * @author jaya sankar
 *
 */

public class Join {
	
	private Table table1;
	private String table1Colum1;
	private Table table2;
	private String table2Colum2;
	
	public Table getTable1() {
	
		return table1;
	}
	
	public void setTable1(Table table1) {
	
		this.table1 = table1;
	}
	
	public String getTable1Colum1() {
	
		return table1Colum1;
	}
	
	public void setTable1Colum1(String table1Colum1) {
	
		this.table1Colum1 = table1Colum1;
	}
	
	public Table getTable2() {
	
		return table2;
	}
	
	public void setTable2(Table table2) {
	
		this.table2 = table2;
	}
	
	public String getTable2Colum2() {
	
		return table2Colum2;
	}
	
	public void setTable2Colum2(String table2Colum2) {
	
		this.table2Colum2 = table2Colum2;
	}
	
}
