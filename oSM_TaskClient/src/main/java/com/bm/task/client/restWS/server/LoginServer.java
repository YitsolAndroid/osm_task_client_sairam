package com.bm.task.client.restWS.server;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.blue.matrix.task.client.connection.CheckConnectivity;
import com.bm.task.client.utils.CommonPopUp;
import com.bm.task.client.utils.LoggerUtils;
import com.bm.task.client.utils.NextActivity;
import com.example.osm_xmlapi.R;

@SuppressLint("ClickableViewAccessibility")
public class LoginServer extends AsyncTask<String, Void, String> {

    ProgressDialog progressDialog;
    String result;
    Context context;
    JSONObject json;
    private String encoded;
    private String userName;
    private String password;
    private int status;
    private static String TAG = LoginServer.class.getSimpleName();


    public LoginServer(Context context, String encoded, String userName, String password) {

        this.encoded = encoded;
        this.context = context;
        this.userName = userName;
        this.password = password;

    }

    protected void onPreExecute() {

        progressDialog = new ProgressDialog(context, R.style.MyTheme);
        progressDialog.setCancelable(false);
        progressDialog
                .setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();

    }

    @Override
    protected String doInBackground(String... urls) {
        try {
            LoggerUtils.error(LoginServer.class.getSimpleName(),":::::::::::Coming Here :::::::::::::");
            System.out.println(":::::::::::Coming Here :::::::::::::");
            System.out.println(":::::::::::" + encoded + " :::::::::::::");
            // make web service connection
            HttpPost request = new HttpPost(urls[0]);
            request.setHeader("Auth", encoded);
            request.setHeader("Accept", "application/json");
            request.setHeader("Content-type", "application/json");
            request.setHeader("Accept-Encoding", "gzip");
            // Build JSON string
            JSONStringer TestApp = new JSONStringer().object()
                    .key("publicIP").value(CheckConnectivity.publicIP)
                    .key("port").value(CheckConnectivity.port)
                    .endObject();
            Log.i(TAG, "ip is =" + CheckConnectivity.publicIP + ", \n port is =" + CheckConnectivity.port);
            StringEntity entity = new StringEntity(TestApp.toString());

            request.setEntity(entity);

            // Log.d("****Parameter Input****", "Testing:" + TestApp);
            StringBuilder sb = new StringBuilder();
            // Send request to WCF service
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpResponse response = httpClient.execute(request);
            LoggerUtils.error(LoginServer.class.getSimpleName(),"login response code is "+response.getStatusLine());
            if (response.getStatusLine().toString().equalsIgnoreCase("HTTP/1.1 200 OK")) {
                // Get the status of web service
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        response.getEntity().getContent()));
                // print status in log
                String line = br.readLine();
                sb.append(line);
                result = sb.toString();
            } else {

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("status", response.getStatusLine().toString());
                result = jsonObject.toString();
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public void onPostExecute(String result) {

        progressDialog.dismiss();

        System.out.println("*******----" + result);

        try {
            JSONObject json = new JSONObject(result);
            status = json.getInt("status");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if (status == 0) {
            Intent intent = new Intent(context,
                    NextActivity.class);
            intent.putExtra("user", userName);
            intent.putExtra("password", password);
            context.startActivity(intent);
        } else {
            CommonPopUp commonPopUp = new CommonPopUp(
                    context,
                    "\n  Invalid UserName & Password \n");
            commonPopUp.setCanceledOnTouchOutside(true);
            commonPopUp.show();
        }


    }
}

