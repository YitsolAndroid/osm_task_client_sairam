
package com.blue.matrix.task.client.restws.dynamicsql;

import com.blue.matrix.task.client.filter.AuthenticationFilter;
import com.blue.matrix.task.client.util.RestfulJsonUtil;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

// Referenced classes of package com.blue.matrix.task.client.restws.dynamicsql:
//            Setter
@Path("/dynamicSql")
public class DynamicSqlQuery {
	
	final static Logger logger = Logger.getLogger(DynamicSqlQuery.class);
	
	private JSONObject jsonOutputObject;
	private JSONObject jsonInputObject;
	private JSONArray jsonTables;
	private JSONArray jsonJoinList;
	private JSONArray jsonRJoinList;
	private JSONArray jsongroupList;
	private JSONArray jsonCriteriaList;
	
	public DynamicSqlQuery ( ) {
	
		jsonTables = null;
		jsonJoinList = null;
		jsonRJoinList = null;
		jsongroupList = null;
		jsonCriteriaList = null;
	}
	
	@POST
	@Path("/query")
	@Produces("application/json")
	@Consumes("application/json")
	public Response dynamicSqlQuery(InputStream incommingJsonObj) {
	
		ArrayList outputArray = null;
		try {
			jsonInputObject = new JSONObject(
					RestfulJsonUtil.convertInputDataToJsonString(incommingJsonObj));
			Setter st = new Setter();
			st.setter(jsonInputObject);
			outputArray = st.call();
			
		}
		catch (Exception e) {
			logger.error(e);
		}
		try {
			incommingJsonObj.close();
		}
		catch (IOException e) {
			logger.error(e);
		}
		
		return Response.ok(outputArray, "application/json").build();
	}
}
