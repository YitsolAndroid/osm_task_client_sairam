
package com.blue.matrix.task.client.model;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author jaya sankar
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class ModifyRemark {
	
	@XmlElement(name = "OrderID")
	private String orderId;
	
	@XmlElement(name = "OrderHistID")
	private String orderHistId;
	
	@XmlElement(name = "RemarkID")
	private String remarkId;
	
	@XmlElement(name = "Text")
	private String text;
	
	public String getOrderId() {
	
		return orderId;
	}
	
	public void setOrderId(String orderId) {
	
		this.orderId = orderId;
	}
	
	public String getOrderHistId() {
	
		return orderHistId;
	}
	
	public void setOrderHistId(String orderHistId) {
	
		this.orderHistId = orderHistId;
	}
	
	public String getRemarkId() {
	
		return remarkId;
	}
	
	public void setRemarkId(String remarkId) {
	
		this.remarkId = remarkId;
	}
	
	public String getText() {
	
		return text;
	}
	
	public void setText(String text) {
	
		this.text = text;
	}
	
}
