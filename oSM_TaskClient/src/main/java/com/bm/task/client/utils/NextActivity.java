/*
* Copyright (C) 2015, 2016, Blue Matrix and/or its affiliates. All rights reserved. 
 */
package com.bm.task.client.utils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;

import com.bm.task.client.fragment.FragmentDisplayActivity;
import com.example.osm_xmlapi.R;

public class NextActivity extends Activity {

	public static String userName;
	public static String password;
	
	private LinearLayout ll_worklist;
	private LinearLayout ll_reporting;
	private LinearLayout ll_query;

	private ConnectionDetector connectionDetector;

	private ViewGroup root;
	private LinearLayout ll_notifications;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.select_page);
		userName = getIntent().getExtras().getString("user");
		password = getIntent().getExtras().getString("password");
		
		//userName = "admin";
		//password = "Osmadmin1";

		ll_worklist = (LinearLayout) findViewById(R.id.ll_worklist);
		ll_reporting = (LinearLayout) findViewById(R.id.ll_reporting);
		ll_query = (LinearLayout) findViewById(R.id.ll_query);
		ll_notifications = (LinearLayout) findViewById(R.id.ll_notifications);
		
		root = (ViewGroup)findViewById(R.id.ll_next);
		new FontType(getApplicationContext(), root);
		
		root = (ViewGroup)findViewById(R.id.ll_heading);
		new FontType(getApplicationContext(), root);
		


		connectionDetector = new ConnectionDetector(getApplicationContext());
		
		ll_notifications.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (!connectionDetector.isConnectingToInternet()) {
					CommonPopUp commonPopUp = new CommonPopUp(
							NextActivity.this, "Please activate internet...");
					commonPopUp.setCanceledOnTouchOutside(true);
					commonPopUp.show();
				} else {
					
					Intent intent = new Intent(NextActivity.this,
							FragmentDisplayActivity.class);
					intent.putExtra("selected", "notifications");
					intent.putExtra("flag", 1);
					startActivity(intent);
				}

			}
		});

		ll_reporting.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (!connectionDetector.isConnectingToInternet()) {
					CommonPopUp commonPopUp = new CommonPopUp(
							NextActivity.this, "Please activate internet...");
					commonPopUp.setCanceledOnTouchOutside(true);
					commonPopUp.show();
				} else {
					
					Intent intent = new Intent(NextActivity.this,
							FragmentDisplayActivity.class);
					intent.putExtra("selected", "report");
					intent.putExtra("flag", 1);
					startActivity(intent);
				}

			}
		});

		ll_worklist.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (!connectionDetector.isConnectingToInternet()) {
					CommonPopUp commonPopUp = new CommonPopUp(
							NextActivity.this, "Please activate internet...");
					commonPopUp.setCanceledOnTouchOutside(true);
					commonPopUp.show();
				} else {

					Intent intent = new Intent(NextActivity.this,
							FragmentDisplayActivity.class);
					intent.putExtra("selected", "worklist");
					intent.putExtra("flag", 1);
					startActivity(intent);
				}
			}
		});
		
		ll_query.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (!connectionDetector.isConnectingToInternet()) {
					CommonPopUp commonPopUp = new CommonPopUp(
							NextActivity.this, "Please activate internet...");
					commonPopUp.setCanceledOnTouchOutside(true);
					commonPopUp.show();
				} else {

					Intent intent = new Intent(NextActivity.this,
							FragmentDisplayActivity.class);
					intent.putExtra("selected", "query");
					intent.putExtra("flag", 1);
					startActivity(intent);
				}
			}
		});


	}

	public void onBackPressed() {

	}

}
