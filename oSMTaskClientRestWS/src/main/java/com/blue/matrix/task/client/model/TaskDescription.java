/**
 * 
 * @author Sahil Nokhwal
 *
 */

package com.blue.matrix.task.client.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "namespace", "version" })
@XmlRootElement()
public class TaskDescription {
	
	@XmlElement(name = "Namespace")
	private String namespace;
	
	@XmlElement(name = "Version")
	private String version;
	
	public String getVersion() {
	
		return version;
	}
	
	public String getNamespace() {
	
		return namespace;
	}
	
	public void setNamespace(String namespace) {
	
		this.namespace = namespace;
	}
	
	public void setVersion(String version) {
	
		this.version = version;
	}
	
}
