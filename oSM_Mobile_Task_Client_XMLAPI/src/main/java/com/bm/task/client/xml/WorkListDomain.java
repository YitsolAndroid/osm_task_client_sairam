/*
* Copyright (C) 2015, 2016, Blue Matrix and/or its affiliates. All rights reserved. 
 */
package com.bm.task.client.xml;


/**
 * USed for Getter and Setter in java
 * @author Bhavya
 *
 */
public class WorkListDomain {

	public String getOrderState() {
		return orderState;
	}

	public void setOrderState(String orderState) {
		this.orderState = orderState;
	}

	public String getExecutionMode() {
		return executionMode;
	}

	public void setExecutionMode(String executionMode) {
		this.executionMode = executionMode;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getRef_no() {
		return ref_no;
	}

	public void setRef_no(String ref_no) {
		this.ref_no = ref_no;
	}

	public String getNameSpace() {
		return nameSpace;
	}

	public void setNameSpace(String nameSpace) {
		this.nameSpace = nameSpace;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getSeqIds() {
		return seqIds;
	}

	public void setSeqIds(String seqIds) {
		this.seqIds = seqIds;
	}

	public String getHistSeqIds() {
		return histSeqIds;
	}

	public void setHistSeqIds(String histSeqIds) {
		this.histSeqIds = histSeqIds;
	}

	public String getCurrentOrderState() {
		return currentOrderState;
	}

	public void setCurrentOrderState(String currentOrderState) {
		this.currentOrderState = currentOrderState;
	}

	String seqIds;
	String histSeqIds;
	String orderState;
	String executionMode;
	String source;
	String type;
	String ref_no;
	String nameSpace;
	String version;
	String currentOrderState;
	String task;
	String priority;
	String processStatus;
	String expectedDuration;
	String expectedOrderCompletionDate;
	String startDate;
	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getExpectedDuration() {
		return expectedDuration;
	}

	public void setExpectedDuration(String expectedDuration) {
		this.expectedDuration = expectedDuration;
	}

	public String getExpectedOrderCompletionDate() {
		return expectedOrderCompletionDate;
	}

	public void setExpectedOrderCompletionDate(String expectedOrderCompletionDate) {
		this.expectedOrderCompletionDate = expectedOrderCompletionDate;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	String createdDate;


	public String getProcessStatus() {
		return processStatus;
	}

	public void setProcessStatus(String processStatus) {
		this.processStatus = processStatus;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	String nodeName;
	String nodeValue;
	String nodeHead;
	String user;

	
	String resume;
	public String getResume() {
		return resume;
	}

	public void setResume(String resume) {
		this.resume = resume;
	}

	public String getSuspend() {
		return suspend;
	}

	public void setSuspend(String suspend) {
		this.suspend = suspend;
	}

	public String getCancel() {
		return cancel;
	}

	public void setCancel(String cancel) {
		this.cancel = cancel;
	}

	public String getAbort() {
		return abort;
	}

	public void setAbort(String abort) {
		this.abort = abort;
	}

	String suspend;
	String cancel;
	String abort;
	private String numRemarks;
	
	public String getNumRemarks() {
		return numRemarks;
	}

	public void setNumRemarks(String numRemarks) {
		this.numRemarks = numRemarks;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getNodeHead() {
		return nodeHead;
	}

	public void setNodeHead(String nodeHead) {
		this.nodeHead = nodeHead;
	}

	public String getNodeName() {
		return nodeName;
	}

	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}

	public String getNodeValue() {
		return nodeValue;
	}

	public void setNodeValue(String nodeValue) {
		this.nodeValue = nodeValue;
	}
	
	public String getTask() {
		return task;
	}

	public void setTask(String task) {
		this.task = task;
	}

}
