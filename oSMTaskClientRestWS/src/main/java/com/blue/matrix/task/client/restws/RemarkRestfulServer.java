
package com.blue.matrix.task.client.restws;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

//import com.blue.matrix.task.client.RemarkHTTPClient;
import com.blue.matrix.task.client.WebserviceHTTPClient;
import com.blue.matrix.task.client.util.RestfulJsonUtil;

@Path("/orderServices")
public class RemarkRestfulServer {
	
	@Context
	ServletContext ctx;
	
	final static Logger logger = Logger.getLogger(RestFullWebservicesServer.class);
	
	private JSONObject jsonOutputObject;
	private String username;
	private String password;
	private String orderId;
	// private RemarkHTTPClient addRemarkClient = null;
	private JSONObject jsonInputObject;
	
	// setter method for Username, Password, OrderId
	/**
	 * 
	 * @param jsonInputObject
	 * @throws JSONException
	 */
	private void setter(JSONObject jsonInputObject) throws Exception {
	
		username = jsonInputObject.getString("username");
		password = jsonInputObject.getString("password");
		orderId = jsonInputObject.getString("orderId");
	}
	
	/**
	 * 
	 * @param incommingJsonObj
	 * @param ctx
	 * @return
	 */
	// addRemark Restful WebService
	@POST
	@Path("/addRemark")
	@Produces("application/json")
	@Consumes("application/json")
	public String addRemark(InputStream incommingJsonObj) {
	
		String soapMessageAsString = null;
		
		try {
			jsonInputObject = new JSONObject(
					RestfulJsonUtil.convertInputDataToJsonString(incommingJsonObj));
			// here we are getting response from soap webservices
			setter(jsonInputObject);
			
			String text = jsonInputObject.getString("text");
			
			// addRemarkClient = new RemarkHTTPClient();
			// soapMessageAsString = addRemarkClient.addRemark(username,
			// password,orderId, text, ctx);
		}
		catch (Exception e) {
			logger.error(e);
			return "{\"status\":\"500 Exception Arised, No Output Data\"}";
		} finally {
			try {
				incommingJsonObj.close();
			}
			catch (IOException e) {
				
				logger.error(e);
			}
		}
		
		return RestfulJsonUtil.checkSoapMessage(soapMessageAsString);
	}
	
}
