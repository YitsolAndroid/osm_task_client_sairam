
package com.blue.matrix.task.client.restws.xmlapi;

import com.blue.matrix.task.client.connection.OSM_Query;
import com.blue.matrix.task.client.util.RestfulJsonUtil;

import java.io.*;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;

import org.apache.log4j.Logger;
import org.json.*;

@Path("/xml_api")
public class XmlApiCaller {
	
	private static final Logger logger = Logger.getLogger(XmlApiCaller.class);
	private String username;
	private String password;
	private String requestXml;
	private String publicIP;
	private String port;
	private JSONObject jsonOutputObject;
	private JSONObject jsonInputObject;
	private String outputMessageAsString;
	
	@Context
	ContainerRequestContext request;
	
	private void setter(JSONObject jsonInputObject) throws Exception {
	
		username = (String) request.getProperty("username");
		password = (String) request.getProperty("password");
		publicIP = jsonInputObject.getString("publicIP");
		port = jsonInputObject.getString("port");
	}
	
	@POST
	@Path("/getOrder")
	@Produces("application/json")
	@Consumes("application/json")
	public String getOrder(InputStream incommingJsonObj)
	
	{
	
		String orderId = null;
		String accept = null;
		String task = null;
		String user = null;
		JSONObject jsonOutputObject = null;
		
		try {
			StringBuilder inputDataAsStringBuilder = null;
			inputDataAsStringBuilder = new StringBuilder();
			BufferedReader in = new BufferedReader(new InputStreamReader(incommingJsonObj));
			for (String line = null; (line = in.readLine()) != null;) {
				inputDataAsStringBuilder.append(line);
			}
			
			// logger.info("Data Recieved:");
			String jsonString = inputDataAsStringBuilder.toString();
			jsonInputObject = new JSONObject(jsonString);
			logger.info((new StringBuilder()).append(jsonInputObject)
					.append("!!!!!!!!!!!!!!!!!!!!!!!!1").toString());
			setter(jsonInputObject);
			orderId = jsonInputObject.getString("orderId");
			accept = jsonInputObject.getString("accept");
			task = jsonInputObject.getString("task");
			user = jsonInputObject.getString("user");
			OSM_Query osmQuery = new OSM_Query();
			outputMessageAsString = osmQuery.getAcceptOrder(username, password, orderId, accept,
					task, user, publicIP, port);
			
			String xmlString = outputMessageAsString.toString();
			
			logger.info(xmlString);
			
			if (outputMessageAsString == null) {
				// logger.info(outputMessageAsString);
				return "{\"status\":\"204; Request is Recieved,HistSeqId is null\"}";
			}
			
			jsonOutputObject = XML.toJSONObject(xmlString);
			
		}
		catch (JSONException e) {
			logger.error(e);
			return "{\"status\":\"No Output Data; Missing Some Input Value or Error while parsing Input/Output Data\"}";
		}
		catch (Exception e) {
			logger.error(e);
			
		}
		
		logger.info((new StringBuilder(String.valueOf(jsonOutputObject.toString()))).append(
				"Json Output").toString());
		return jsonOutputObject.toString();
	}
	
	@POST
	@Path("/listStatesNStatuses")
	@Produces("application/json")
	@Consumes("application/json")
	public String listStatesNStatuses(InputStream incommingJsonObj)
	
	{
	
		String orderId = null;
		String task = null;
		JSONObject jsonOutputObject = null;
		
		try {
			StringBuilder inputDataAsStringBuilder = null;
			inputDataAsStringBuilder = new StringBuilder();
			BufferedReader in = new BufferedReader(new InputStreamReader(incommingJsonObj));
			for (String line = null; (line = in.readLine()) != null;) {
				inputDataAsStringBuilder.append(line);
			}
			
			// logger.info("Data Recieved:");
			String jsonString = inputDataAsStringBuilder.toString();
			jsonInputObject = new JSONObject(jsonString);
			logger.info((new StringBuilder()).append(jsonInputObject)
					.append("!!!!!!!!!!!!!!!!!!!!!!!!1").toString());
			setter(jsonInputObject);
			orderId = jsonInputObject.getString("orderId");
			task = jsonInputObject.getString("task");
			
		}
		catch (JSONException e) {
			logger.error(e);
			return "{\"status\":\"No Output Data; Missing Some Input Value or Error while parsing Input/Output Data\"}";
		}
		catch (Exception e) {
			logger.error(e);
			
		}
		
		try {
			
			OSM_Query osmQuery = new OSM_Query();
			outputMessageAsString = osmQuery.listStatesNStatuses(username, password, orderId, task,
					publicIP, port);
			
			String xmlString = outputMessageAsString.toString();
			
			logger.info(xmlString + "XML Data------------");
			
			if (outputMessageAsString == null) {
				// logger.info(outputMessageAsString);
				return "{\"status\":\"204; Request is Recieved but not able to give response\"}";
			}
			
			jsonOutputObject = XML.toJSONObject(outputMessageAsString.toString());
			
		}
		
		catch (JSONException e) {
			logger.error(e);
			return "{\"status\":\"No Output Data; Hist ID Not Found\"}";
		}
		catch (Exception e) {
			logger.error(e);
			return "{\"status\":\"500 Exception Arised; Reason: Server is Down, Network Error\"}";
		}
		
		logger.info(jsonOutputObject + "---------------------Json");
		logger.info((new StringBuilder(String.valueOf(jsonOutputObject.toString()))).append(
				"Json Output").toString());
		return jsonOutputObject.toString();
	}
	
	@POST
	@Path("/getAssignOrder")
	@Produces("application/json")
	@Consumes("application/json")
	public String getAssignOrder(InputStream incommingJsonObj)
	
	{
	
		String orderId = null;
		String user = null;
		String task = null;
		
		JSONObject jsonOutputObject = null;
		
		try {
			StringBuilder inputDataAsStringBuilder = null;
			inputDataAsStringBuilder = new StringBuilder();
			BufferedReader in = new BufferedReader(new InputStreamReader(incommingJsonObj));
			for (String line = null; (line = in.readLine()) != null;) {
				inputDataAsStringBuilder.append(line);
			}
			
			// logger.info("Data Recieved:");
			String jsonString = inputDataAsStringBuilder.toString();
			jsonInputObject = new JSONObject(jsonString);
			logger.info((new StringBuilder()).append(jsonInputObject)
					.append("!!!!!!!!!!!!!!!!!!!!!!!!1").toString());
			setter(jsonInputObject);
			orderId = jsonInputObject.getString("orderId");
			user = jsonInputObject.getString("user");
			task = jsonInputObject.getString("task");
			
		}
		catch (JSONException e) {
			logger.error(e);
			return "{\"status\":\"No Output Data; Missing Some Input Value or Error while parsing Input/Output Data\"}";
		}
		catch (Exception e) {
			logger.error(e);
			
		}
		
		try {
			
			OSM_Query osmQuery = new OSM_Query();
			outputMessageAsString = osmQuery.getOrder(username, password, orderId, user, task,
					publicIP, port);
			
			String xmlString = outputMessageAsString.toString();
			
			logger.info(xmlString);
			
			if (outputMessageAsString == null) {
				// logger.info(outputMessageAsString);
				return "{\"status\":\"204; Request is Recieved but not able to give response\"}";
			}
			
			jsonOutputObject = XML.toJSONObject(xmlString);
		}
		
		catch (JSONException e) {
			logger.error(e);
			return "{\"status\":\"No Output Data; Hist ID Not Found\"}";
		}
		catch (Exception e) {
			logger.error(e);
			return "{\"status\":\"500 Exception Arised; Reason: Server is Down, Network Error\"}";
		}
		
		logger.info((new StringBuilder(String.valueOf(jsonOutputObject.toString()))).append(
				"Json Output").toString());
		return jsonOutputObject.toString();
	}
	
	@POST
	@Path("/getCompleteOrder")
	@Produces("application/json")
	@Consumes("application/json")
	public String getCompleteOrder(InputStream incommingJsonObj)
	
	{
	
		String orderId = null;
		String status = null;
		String task = null;
		
		JSONObject jsonOutputObject = null;
		
		try {
			StringBuilder inputDataAsStringBuilder = null;
			inputDataAsStringBuilder = new StringBuilder();
			BufferedReader in = new BufferedReader(new InputStreamReader(incommingJsonObj));
			for (String line = null; (line = in.readLine()) != null;) {
				inputDataAsStringBuilder.append(line);
			}
			
			// logger.info("Data Recieved:");
			String jsonString = inputDataAsStringBuilder.toString();
			jsonInputObject = new JSONObject(jsonString);
			logger.info((new StringBuilder()).append(jsonInputObject)
					.append("!!!!!!!!!!!!!!!!!!!!!!!!1").toString());
			setter(jsonInputObject);
			orderId = jsonInputObject.getString("orderId");
			status = jsonInputObject.getString("status");
			task = jsonInputObject.getString("task");
			
		}
		catch (JSONException e) {
			logger.error(e);
			return "{\"status\":\"No Output Data; Missing Some Input Value or Error while parsing Input/Output Data\"}";
		}
		catch (Exception e) {
			logger.error(e);
			
		}
		
		try {
			
			OSM_Query osmQuery = new OSM_Query();
			outputMessageAsString = osmQuery.getCompleteOrder(username, password, orderId, status,
					task, publicIP, port);
			
			String xmlString = outputMessageAsString.toString();
			
			logger.info(xmlString);
			
			if (outputMessageAsString == null) {
				// logger.info(outputMessageAsString);
				return "{\"status\":\"204; Request is Recieved but not able to give response\"}";
			}
			
			jsonOutputObject = XML.toJSONObject(xmlString);
		}
		
		catch (JSONException e) {
			logger.error(e);
			return "{\"status\":\"No Output Data; Hist ID Not Found\"}";
		}
		catch (Exception e) {
			logger.error(e);
			return "{\"status\":\"500 Exception Arised; Reason: Server is Down, Network Error\"}";
		}
		
		logger.info((new StringBuilder(String.valueOf(jsonOutputObject.toString()))).append(
				"Json Output").toString());
		return jsonOutputObject.toString();
	}
	
	@POST
	@Path("/getReceiveOrder")
	@Produces("application/json")
	@Consumes("application/json")
	public String getReceiveOrder(InputStream incommingJsonObj)
	
	{
	
		String orderId = null;
		String task = null;
		JSONObject jsonOutputObject = null;
		
		try {
			StringBuilder inputDataAsStringBuilder = null;
			inputDataAsStringBuilder = new StringBuilder();
			BufferedReader in = new BufferedReader(new InputStreamReader(incommingJsonObj));
			for (String line = null; (line = in.readLine()) != null;) {
				inputDataAsStringBuilder.append(line);
			}
			
			// logger.info("Data Recieved:");
			String jsonString = inputDataAsStringBuilder.toString();
			jsonInputObject = new JSONObject(jsonString);
			logger.info((new StringBuilder()).append(jsonInputObject)
					.append("!!!!!!!!!!!!!!!!!!!!!!!!1").toString());
			setter(jsonInputObject);
			orderId = jsonInputObject.getString("orderId");
			task = jsonInputObject.getString("task");
			
		}
		catch (JSONException e) {
			logger.error(e);
			return "{\"status\":\"No Output Data; Missing Some Input Value or Error while parsing Input/Output Data\"}";
		}
		catch (Exception e) {
			logger.error(e);
			
		}
		
		try {
			
			OSM_Query osmQuery = new OSM_Query();
			outputMessageAsString = osmQuery.getReceiveOrder(username, password, orderId, task,
					publicIP, port);
			
			String xmlString = outputMessageAsString.toString();
			
			logger.info(xmlString + "XML Data------------");
			
			if (outputMessageAsString == null) {
				// logger.info(outputMessageAsString);
				return "{\"status\":\"204; Request is Recieved but not able to give response\"}";
			}
			
			jsonOutputObject = XML.toJSONObject(outputMessageAsString.toString());
			
		}
		
		catch (JSONException e) {
			logger.error(e);
			return "{\"status\":\"No Output Data; Hist ID Not Found\"}";
		}
		catch (Exception e) {
			logger.error(e);
			return "{\"status\":\"500 Exception Arised; Reason: Server is Down, Network Error\"}";
		}
		
		logger.info(jsonOutputObject + "---------------------Json");
		logger.info((new StringBuilder(String.valueOf(jsonOutputObject.toString()))).append(
				"Json Output").toString());
		return jsonOutputObject.toString();
	}
	
	@POST
	@Path("/getWorklistOrder")
	@Produces("application/json")
	@Consumes("application/json")
	public String getWorklistOrder(InputStream incommingJsonObj)
	
	{
	
		JSONObject jsonOutputObject = null;
		try {
			StringBuilder inputDataAsStringBuilder = null;
			inputDataAsStringBuilder = new StringBuilder();
			BufferedReader in = new BufferedReader(new InputStreamReader(incommingJsonObj));
			for (String line = null; (line = in.readLine()) != null;) {
				inputDataAsStringBuilder.append(line);
			}
			
			// logger.info("Data Recieved:");
			String jsonString = inputDataAsStringBuilder.toString();
			jsonInputObject = new JSONObject(jsonString);
			logger.info((new StringBuilder()).append(jsonInputObject)
					.append("!!!!!!!!!!!!!!!!!!!!!!!!1").toString());
			setter(jsonInputObject);
			
		}
		catch (JSONException e) {
			logger.error(e);
			return "{\"status\":\"No Output Data; Missing Some Input Value or Error while parsing Input/Output Data\"}";
		}
		catch (Exception e) {
			logger.error(e);
			
		}
		
		try {
			
			OSM_Query osmQuery = new OSM_Query();
			outputMessageAsString = osmQuery.getWorklistOrder(username, password, publicIP, port);
			
			if (outputMessageAsString == null) {
				// logger.info(outputMessageAsString);
				return "{\"status\":\"204; Request is Recieved but not able to give response\"}";
			}
			
			jsonOutputObject = XML.toJSONObject(outputMessageAsString.toString());
			
		}
		
		catch (JSONException e) {
			logger.error(e);
			return "{\"status\":\"No Output Data; Hist ID Not Found\"}";
		}
		catch (Exception e) {
			logger.error(e);
			return "{\"status\":\"500 Exception Arised; Reason: Server is Down, Network Error\"}";
		}
		
		logger.info((new StringBuilder(String.valueOf(jsonOutputObject.toString()))).append(
				"Json Output").toString());
		return jsonOutputObject.toString();
	}
	
}
