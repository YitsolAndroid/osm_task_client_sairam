/*
* Copyright (C) 2015, 2016, Blue Matrix and/or its affiliates. All rights reserved. 
 */
package com.bm.task.client.utils;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bm.task.client.xml.WorkListDomain;
import com.example.osm_xmlapi.R;

public class EditNodeDialog extends Dialog 
{
	private Context context;
	
	private Button okbutton,cancelbutton;
	
	private EditText edtnote;
	
	private String addvalue="NA";
	
	private EditNodeListner listner;
	
	
	
	public EditNodeDialog(final Context context, final WorkListDomain model) {
		// TODO Auto-generated constructor stub
		super(context);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		this.context=context;
		
		setContentView(R.layout.nodevaluedialog);
		
		new ConnectionDetector(context);
		
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
		
		okbutton=(Button) findViewById(R.id.profile_dialog_ok);
		
		cancelbutton=(Button) findViewById(R.id.profile_dialog_cancel);
		
		edtnote=(EditText) findViewById(R.id.editText1);
		
		edtnote.setText(model.getNodeValue());
		okbutton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				addvalue=edtnote.getText().toString();
				
				if(addvalue.equalsIgnoreCase(""))
				{
					Toast.makeText(context, "Please Enter the Value", Toast.LENGTH_SHORT).show();
					
					edtnote.requestFocus();
				}
				else
				{
					model.setNodeValue(addvalue);
					listner.getEditNodeText(addvalue);
					hidevirtualkeyboardinfragment();
				}
				dismiss();
				
				
			}
		});
		
		cancelbutton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				hidevirtualkeyboardinfragment();
				dismiss();
				
			}
		});
		
		
	}
	
	public void hidevirtualkeyboardinfragment()
	{
		InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(edtnote.getWindowToken(), 0);

	}
	
	public void registerlistner(EditNodeListner listner)
	{
		this.listner=listner;
		
		
	}

}
