
package com.blue.matrix.task.client.connection;

import java.net.HttpURLConnection;
import java.net.URL;

public class CheckConnectivity {

	public static String publicIP = "192.168.0.170";
	//public static String publicIP = "192.168.0.102";
	public static String port = "8082";
	//public static String port = "8080";
	public static Boolean result;
	
	public CheckConnectivity ( ) {
	
	}
	
	public Boolean Connectivity() throws Exception {
		
		URL url = new URL((new StringBuilder("http://")).append(publicIP).append(":").append(port)
				.append("/OrderManagement/").toString());
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setConnectTimeout(1000);
		if (connection.getHeaderField(0) == null) {
			result = Boolean.valueOf(false);
		}
		else {
			result = Boolean.valueOf(true);
		}
		return result;
	}
	
}
