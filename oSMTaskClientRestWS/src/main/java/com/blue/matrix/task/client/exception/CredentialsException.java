
package com.blue.matrix.task.client.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * 
 * @author jaya sankar
 *
 */

public class CredentialsException extends WebApplicationException {
	
	public CredentialsException ( ) {
	
		super(Response.status(Response.Status.INTERNAL_SERVER_ERROR).build());
	}
	
	/**
	 * 
	 * @param message
	 */
	public CredentialsException (String message ) {
	
		super(Response.status(Response.Status.UNAUTHORIZED).entity(message)
				.type(MediaType.APPLICATION_JSON).build());
	}
	
}
