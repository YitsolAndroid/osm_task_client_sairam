
package com.blue.matrix.task.client.connection;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;

import javax.ws.rs.core.Response;

import org.json.JSONObject;

//import weblogic.auddi.util.Logger;

import com.blue.matrix.task.client.DB_Connection;
import com.blue.matrix.task.client.model.HistSeqID;
import com.blue.matrix.task.client.restws.RestFullSQLWebServices;

public class OSM_Query {
	
	public static String cookie;
	public static StringBuffer response;
	public static StringBuffer xmlResponse;
	public static String username;
	public static String password;
	public static String requestXml;
	private String publicIP;
	private String port;
	private String orderId;
	private String histSeqId;
	private String Result;
	private String status;
	private String user;
	
	public String getAcceptOrder(String username, String password, String orderId, String accept,
			String task, String user, String publicIP, String port) throws Exception {
	
		RestFullSQLWebServices sqlWS = new RestFullSQLWebServices();
		DB_Connection DB = new DB_Connection();
		HistSeqID histId = DB.getHistSeqID(orderId, task);
		
		String histSeqId = histId.getHistSeqId();
		if (histSeqId == null) {
			return null;
		}
		//Logger.info("HIST ID:" + histSeqId);
		String s1 = "<GetOrder.Request xmlns=\"urn:com:metasolv:oms:xmlapi:1\">";
		String s2 = "<OrderID>" + orderId + "</OrderID>";
		String s3 = "<Accept>" + accept + "</Accept>";
		String s4 = "<OrderHistID>" + histSeqId + "</OrderHistID>";
		String s5 = "</GetOrder.Request>";
		
		requestXml = s1 + s2 + s3 + s4 + s5;
		System.out.println(requestXml);
		String str = (new StringBuilder(String.valueOf(URLEncoder.encode("username")))).append("=")
				.append(URLEncoder.encode(username)).append("&")
				.append(URLEncoder.encode("password")).append("=")
				.append(URLEncoder.encode(password)).toString();
		CheckConnectivity checkConnectivity = new CheckConnectivity();
		boolean connectivity = checkConnectivity.Connectivity().booleanValue();
		System.out.println((new StringBuilder(String.valueOf(connectivity))).append(
				"connectivity------------------------").toString());
		if (connectivity) {
			HttpURLConnection connection = connectionBuilder(publicIP, port, str);
			int code = connection.getResponseCode();
			connectionFinder(publicIP, port, connection, code);
		}
		else {
			Result = "Failure";
		}
		System.out.println((new StringBuilder("Response recieved from DB server")).append(
				xmlResponse).toString());
		System.out.println(Result);
		return xmlResponse.toString();
	}
	
	/**
	 * @param publicIP
	 * @param port
	 * @return
	 * @throws MalformedURLException
	 * @throws IOException
	 * @throws ProtocolException
	 */
	private HttpURLConnection connectOutput(String publicIP, String port)
			throws MalformedURLException, IOException, ProtocolException {
	
		HttpURLConnection connection;
		OutputStream out;
		BufferedReader in;
		String inputLine;
		URL xmlApiUrl = new URL((new StringBuilder("http://")).append(publicIP).append(":")
				.append(port).append("/OrderManagement/XMLAPI/XMLAPI").toString());
		connection = (HttpURLConnection) xmlApiUrl.openConnection();
		System.out.println((new StringBuilder("Request: ")).append(requestXml).toString());
		connection.setAllowUserInteraction(false);
		connection.setDoOutput(true);
		connection.setDoInput(true);
		connection.setRequestMethod("POST");
		connection.addRequestProperty("Cookie", cookie);
		connection.setRequestProperty("Content-Type", "text/xml");
		connection.connect();
		out = connection.getOutputStream();
		out.write(requestXml.getBytes());
		out.close();
		connection.getResponseMessage();
		in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		System.out.println((new StringBuilder("Request: ")).append(requestXml).toString());
		xmlResponse = new StringBuffer();
		while ((inputLine = in.readLine()) != null) {
			xmlResponse.append(inputLine);
		}
		System.out.println((new StringBuilder("Response from OSM XML:")).append(xmlResponse)
				.toString());
		in.close();
		return connection;
	}
	
	/**
	 * @param publicIP
	 * @param port
	 * @return
	 * @throws MalformedURLException
	 * @throws IOException
	 * @throws ProtocolException
	 */
	private HttpURLConnection connect(String publicIP, String port) throws MalformedURLException,
			IOException, ProtocolException {
	
		URL url;
		HttpURLConnection connection;
		url = new URL((new StringBuilder("http://")).append(publicIP).append(":").append(port)
				.append("/OrderManagement/XMLAPI/logout").toString());
		System.out.println((new StringBuilder("URL :")).append(url).toString());
		connection = (HttpURLConnection) url.openConnection();
		connection.setAllowUserInteraction(false);
		connection.setDoOutput(true);
		connection.setDoInput(true);
		connection.setRequestMethod("POST");
		connection.addRequestProperty("Cookie", cookie);
		connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		connection.setRequestProperty("Content-Length", "0");
		connection.connect();
		return connection;
	}
	
	public String listStatesNStatuses(String username, String password, String orderId,
			String task, String publicIP, String port) throws Exception {
	
		RestFullSQLWebServices sqlWS = new RestFullSQLWebServices();
		
		Response responseHistSeqId = sqlWS.getHistSeqID(orderId, task);
		String jsonHist = responseHistSeqId.getEntity().toString();
		JSONObject jsonResponseHistSeqId = new JSONObject(jsonHist);
		System.out.println((new StringBuilder("Hist Seq Id  Recieved: -------")).append(
				jsonResponseHistSeqId).toString());
		String histSeqId = jsonResponseHistSeqId.getString("histSeqId");
		String s1 = "<ListStatesNStatuses.Request xmlns=\"urn:com:metasolv:oms:xmlapi:1\">";
		String s2 = "<OrderID>" + orderId + "</OrderID>";
		String s3 = "<OrderHistID>" + histSeqId + "</OrderHistID>";
		String s4 = "</ListStatesNStatuses.Request>";
		requestXml = s1 + s2 + s3 + s4;
		System.out.println(requestXml);
		String str = (new StringBuilder(String.valueOf(URLEncoder.encode("username")))).append("=")
				.append(URLEncoder.encode(username)).append("&")
				.append(URLEncoder.encode("password")).append("=")
				.append(URLEncoder.encode(password)).toString();
		CheckConnectivity checkConnectivity = new CheckConnectivity();
		boolean connectivity = checkConnectivity.Connectivity().booleanValue();
		System.out.println((new StringBuilder(String.valueOf(connectivity))).append(
				"connectivity------------------------").toString());
		if (connectivity) {
			HttpURLConnection connection = connectionBuilder(publicIP, port, str);
			int code = connection.getResponseCode();
			connectionFinder(publicIP, port, connection, code);
		}
		else {
			Result = "Failure";
		}
		System.out.println((new StringBuilder("Response recieved from DB server")).append(
				xmlResponse).toString());
		System.out.println(Result);
		return xmlResponse.toString();
	}
	
	public String getOrder(String username, String password, String orderId, String user,
			String task, String publicIP, String port) throws Exception {
	
		RestFullSQLWebServices sqlWS = new RestFullSQLWebServices();
		
		Response responseHistSeqId = sqlWS.getHistSeqID(orderId, task);
		String jsonHist = responseHistSeqId.getEntity().toString();
		JSONObject jsonResponseHistSeqId = new JSONObject(jsonHist);
		String histSeqId = jsonResponseHistSeqId.getString("histSeqId");
		assignOrderBuilder(orderId, user, histSeqId);
		String str = (new StringBuilder(String.valueOf(URLEncoder.encode("username")))).append("=")
				.append(URLEncoder.encode(username)).append("&")
				.append(URLEncoder.encode("password")).append("=")
				.append(URLEncoder.encode(password)).toString();
		CheckConnectivity checkConnectivity = new CheckConnectivity();
		boolean connectivity = checkConnectivity.Connectivity().booleanValue();
		System.out.println((new StringBuilder(String.valueOf(connectivity))).append(
				"connectivity------------------------").toString());
		if (connectivity) {
			HttpURLConnection connection = connectionBuilder(publicIP, port, str);
			int code = connection.getResponseCode();
			connectionFinder(publicIP, port, connection, code);
		}
		else {
			Result = "Failure";
		}
		System.out.println((new StringBuilder("Response recieved from DB server")).append(
				xmlResponse).toString());
		System.out.println(Result);
		return xmlResponse.toString();
	}
	
	/**
	 * @param orderId
	 * @param user
	 * @param histSeqId
	 */
	private void assignOrderBuilder(String orderId, String user, String histSeqId) {
	
		System.out.println((new StringBuilder("Hist Seq ID : ------------")).append(histSeqId)
				.toString());
		String s1 = "<AssignOrder.Request xmlns=\"urn:com:metasolv:oms:xmlapi:1\">";
		String s2 = (new StringBuilder("<OrderID>")).append(orderId).append("</OrderID>")
				.toString();
		String s3 = (new StringBuilder("<OrderHistID>")).append(histSeqId).append("</OrderHistID>")
				.toString();
		String s4 = (new StringBuilder("<User>")).append(user).append("</User>").toString();
		String s5 = "</AssignOrder.Request>";
		requestXml = (new StringBuilder(String.valueOf(s1))).append(s2).append(s3).append(s4)
				.append(s5).toString();
		System.out.println(requestXml);
	}
	
	public String getCompleteOrder(String username, String password, String orderId, String status,
			String task, String publicIP, String port) throws Exception {
	
		RestFullSQLWebServices sqlWS = new RestFullSQLWebServices();
		// Response responseView = sqlWS.getView(orderId);
		// String jsonView = responseView.getEntity().toString();
		// JSONObject jsonResponseView = new JSONObject(jsonView);
		// System.out.println((new
		// StringBuilder("View Recieved: -------")).append(jsonResponseView).toString());
		// String view = jsonResponseView.getString("viewName");
		Response responseHistSeqId = sqlWS.getHistSeqID(orderId, task);
		
		System.out.println(responseHistSeqId + "-------------------------------");
		
		String jsonHist = responseHistSeqId.getEntity().toString();
		
		System.out.println(jsonHist + "-------------------------------");
		
		JSONObject jsonResponseHistSeqId = new JSONObject(jsonHist);
		System.out.println((new StringBuilder("Hist Seq Id  Recieved: -------")).append(
				jsonResponseHistSeqId).toString());
		String histSeqId = jsonResponseHistSeqId.getString("histSeqId");
		String s1 = "<CompleteOrder.Request xmlns=\"urn:com:metasolv:oms:xmlapi:1\">";
		String s2 = (new StringBuilder("<OrderID>")).append(orderId).append("</OrderID>")
				.toString();
		String s3 = (new StringBuilder("<OrderHistID>")).append(histSeqId).append("</OrderHistID>")
				.toString();
		String s4 = (new StringBuilder("<Status>")).append(status).append("</Status>").toString();
		String s5 = "</CompleteOrder.Request>";
		requestXml = (new StringBuilder(String.valueOf(s1))).append(s2).append(s3).append(s4)
				.append(s5).toString();
		System.out.println(requestXml);
		String str = (new StringBuilder(String.valueOf(URLEncoder.encode("username")))).append("=")
				.append(URLEncoder.encode(username)).append("&")
				.append(URLEncoder.encode("password")).append("=")
				.append(URLEncoder.encode(password)).toString();
		CheckConnectivity checkConnectivity = new CheckConnectivity();
		boolean connectivity = checkConnectivity.Connectivity().booleanValue();
		System.out.println((new StringBuilder(String.valueOf(connectivity))).append(
				"connectivity------------------------").toString());
		if (connectivity) {
			HttpURLConnection connection = connectionBuilder(publicIP, port, str);
			int code = connection.getResponseCode();
			connectionFinder(publicIP, port, connection, code);
		}
		else {
			Result = "Failure";
		}
		System.out.println((new StringBuilder("Response recieved from DB server")).append(
				xmlResponse).toString());
		System.out.println(Result);
		return xmlResponse.toString();
	}
	
	/**
	 * @param publicIP
	 * @param port
	 * @param connection
	 * @param code
	 * @throws Exception
	 * @throws IOException
	 * @throws MalformedURLException
	 * @throws ProtocolException
	 */
	private void connectionFinder(String publicIP, String port, HttpURLConnection connection,
			int code) throws Exception, IOException, MalformedURLException, ProtocolException {
	
		if (code == 200) {
			getCookiee(connection);
		}
		else {
			throw new Exception((new StringBuilder("HTTP response code != 200 OK :")).append(code)
					.toString());
		}
		BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		response = new StringBuffer();
		String inputLine;
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		System.out.println((new StringBuilder()).append(response)
				.append("Response -----------------------------").toString());
		in.close();
		connection.disconnect();
		connection = connectOutput(publicIP, port);
		connection.disconnect();
		connection = connect(publicIP, port);
		code = connection.getResponseCode();
		if (code != 200) {
			throw new Exception((new StringBuilder("HTTP response code != 200 OK :")).append(code)
					.toString());
		}
		in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		response = new StringBuffer();
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		connection.disconnect();
	}
	
	public String getReceiveOrder(String username, String password, String orderId, String task,
			String publicIP, String port) throws Exception {
	
		RestFullSQLWebServices sqlWS = new RestFullSQLWebServices();
		
		Response responseHistSeqId = sqlWS.getHistSeqID(orderId, task);
		String jsonHist = responseHistSeqId.getEntity().toString();
		JSONObject jsonResponseHistSeqId = new JSONObject(jsonHist);
		System.out.println((new StringBuilder("Hist Seq Id  Recieved: -------")).append(
				jsonResponseHistSeqId).toString());
		String histSeqId = jsonResponseHistSeqId.getString("histSeqId");
		String s1 = "<ReceiveOrder.Request xmlns=\"urn:com:metasolv:oms:xmlapi:1\">";
		String s2 = "<OrderID>" + orderId + "</OrderID>";
		String s3 = "<OrderHistID>" + histSeqId + "</OrderHistID>";
		String s4 = "</ReceiveOrder.Request>";
		requestXml = s1 + s2 + s3 + s4;
		System.out.println(requestXml);
		String str = (new StringBuilder(String.valueOf(URLEncoder.encode("username")))).append("=")
				.append(URLEncoder.encode(username)).append("&")
				.append(URLEncoder.encode("password")).append("=")
				.append(URLEncoder.encode(password)).toString();
		CheckConnectivity checkConnectivity = new CheckConnectivity();
		boolean connectivity = checkConnectivity.Connectivity().booleanValue();
		System.out.println((new StringBuilder(String.valueOf(connectivity))).append(
				"connectivity------------------------").toString());
		if (connectivity) {
			HttpURLConnection connection = connectionBuilder(publicIP, port, str);
			int code = connection.getResponseCode();
			connectionFinder(publicIP, port, connection, code);
		}
		else {
			Result = "Failure";
		}
		System.out.println((new StringBuilder("Response recieved from DB server")).append(
				xmlResponse).toString());
		System.out.println(Result);
		return xmlResponse.toString();
	}
	
	/**
	 * @param connection
	 * @throws Exception
	 */
	private void getCookiee(HttpURLConnection connection) throws Exception {
	
		String receivedcookie = connection.getHeaderField("Set-Cookie");
		if (receivedcookie == null) {
			throw new Exception("Server did not return session cookie");
		}
		cookie = receivedcookie.substring(0, receivedcookie.indexOf(';'));
	}
	
	public String getWorklistOrder(String username, String password, String publicIP, String port)
			throws Exception {
	
		String s1 = "<Worklist.Request xmlns=\"urn:com:metasolv:oms:xmlapi:1\">";
		String s2 = "<OrderBy>";
		String s3 = "<Field order=\"descending\">_order_seq_id</Field>";
		String s4 = "</OrderBy>";
		String s5 = "</Worklist.Request>";
		requestXml = (new StringBuilder(String.valueOf(s1))).append(s2).append(s3).append(s4)
				.append(s5).toString();
		System.out.println(requestXml);
		String str = (new StringBuilder(String.valueOf(URLEncoder.encode("username")))).append("=")
				.append(URLEncoder.encode(username)).append("&")
				.append(URLEncoder.encode("password")).append("=")
				.append(URLEncoder.encode(password)).toString();
		CheckConnectivity checkConnectivity = new CheckConnectivity();
		boolean connectivity = checkConnectivity.Connectivity().booleanValue();
		System.out.println((new StringBuilder(String.valueOf(connectivity))).append(
				"connectivity------------------------").toString());
		if (connectivity) {
			HttpURLConnection connection = connectionBuilder(publicIP, port, str);
			int code = connection.getResponseCode();
			connectionFinder(publicIP, port, connection, code);
		}
		else {
			Result = "Failure";
		}
		System.out.println(xmlResponse);
		System.out.println(Result);
		return xmlResponse.toString();
	}
	
	/**
	 * @param publicIP
	 * @param port
	 * @param str
	 * @return
	 * @throws MalformedURLException
	 * @throws IOException
	 * @throws ProtocolException
	 */
	private HttpURLConnection connectionBuilder(String publicIP, String port, String str)
			throws MalformedURLException, IOException, ProtocolException {
	
		Result = "Success";
		CheckConnectivity.publicIP = publicIP;
		CheckConnectivity.port = port;
		byte bytes[] = str.getBytes();
		URL url = new URL((new StringBuilder("http://")).append(publicIP).append(":").append(port)
				.append("/OrderManagement/XMLAPI/login").toString());
		System.out.println((new StringBuilder()).append(url).append("URL-------------------")
				.toString());
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setConnectTimeout(1000);
		connection.setAllowUserInteraction(false);
		connection.setDoOutput(true);
		connection.setDoInput(true);
		connection.setRequestMethod("POST");
		connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		connection.setRequestProperty("Content-Length", String.valueOf(bytes.length));
		connection.connect();
		OutputStream out = connection.getOutputStream();
		out.write(bytes);
		out.close();
		return connection;
	}
}
