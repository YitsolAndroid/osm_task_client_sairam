package com.bm.task.client.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.annotation.SuppressLint;
import android.content.Context;

@SuppressLint("SimpleDateFormat")
public class CurrentDate {
	public String currentDate,kotdate,billdate;
	public String fullDateTime,slashDate,onlytime,onlyMonth;
	public String dateifon;
	    
	    public CurrentDate(Context context)
	{
		SimpleDateFormat sdf1 = new SimpleDateFormat("ddMMMyy");
		currentDate = sdf1.format(new Date());
		
		
		
		SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
		fullDateTime = sdf2.format(new Date());
		
		SimpleDateFormat sdf3 = new SimpleDateFormat("dd/MM/yyyy");
		slashDate = sdf3.format(new Date());
		
		SimpleDateFormat sdf4 = new SimpleDateFormat("h:mm a");
		onlytime = sdf4.format(new Date());
		
		SimpleDateFormat sdf5=new SimpleDateFormat("ddMMyy");
		kotdate=sdf5.format(new Date());
		
		SimpleDateFormat sdf6=new SimpleDateFormat("yyMMdd");
		billdate=sdf6.format(new Date());
		
		SimpleDateFormat sdf7 = new SimpleDateFormat("hh:mm:ss");
		fullDateTime = sdf7.format(new Date());
		
		SimpleDateFormat sdf8 = new SimpleDateFormat("ddMMMyy");
		currentDate = sdf8.format(new Date());  
		
		SimpleDateFormat sdf9 = new SimpleDateFormat("MM");
		onlyMonth = sdf9.format(new Date());
		
		SimpleDateFormat sdf10 = new SimpleDateFormat("yyyy-MM-dd");
		dateifon = sdf10.format(new Date());
		
	}
	
	
}
