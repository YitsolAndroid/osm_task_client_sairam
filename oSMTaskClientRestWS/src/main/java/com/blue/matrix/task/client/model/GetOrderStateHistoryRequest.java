
package com.blue.matrix.task.client.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author jaya sankar
 *
 */

@XmlRootElement
public class GetOrderStateHistoryRequest {
	
	@XmlElement(name = "OrderID")
	public String orderId;
	
}
