
package com.blue.matrix.task.client.restws;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import com.blue.matrix.task.client.DB_Connection;
import com.blue.matrix.task.client.model.AllOrderStates;
import com.blue.matrix.task.client.model.Catridges;
import com.blue.matrix.task.client.model.HistSeqID;
import com.blue.matrix.task.client.model.OrderStatus;
import com.blue.matrix.task.client.model.Process;
import com.blue.matrix.task.client.model.ProcessofOrders;
import com.blue.matrix.task.client.model.Status;
import com.blue.matrix.task.client.model.View;

/**
 * 
 * @author Sahil Nokhwal
 * 
 * @FileName: RestFullWebservicesServer.java
 * 
 * @version: 1.0.0.0.0
 * 
 * @Descrption This class is use for executing queries on DB through Restful
 *             Webservices
 *
 */

@Path("/orderServices")
public class RestFullSQLWebServices {
	
	private static DB_Connection dbConnection;
	
	private String orderId;
	
	final static Logger logger = Logger.getLogger(DB_Connection.class);
	
	public static DB_Connection getDatabase() throws SQLException {
	
		if (dbConnection == null) {
			dbConnection = new DB_Connection();
			return dbConnection;
		}
		else
			return dbConnection;
	}
	
	/**
	 * 
	 * This service method is used for getting Process Status Mnemonic
	 * 
	 * @return Json Data
	 * @throws Exception
	 */
	@GET
	@Path("/getProcessStatusMnemonic")
	@Produces("application/json")
	public Response getProcessStatusMnemonic() {
	
		ArrayList<Process> process = null;
		
		try {
			
			process = (ArrayList<Process>) getDatabase().getProcessStatusMnemonic();
			
			logger.info(process);
		}
		
		catch (SQLException e) {
			logger.error(e);
			return Response
					.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("{\"status\":\"500 Internal Server Error; Reason: Server Out of Reach\"}")
					.build();
		}
		
		String outputJson = new JSONArray(process).toString();
		
		return Response.ok(outputJson, MediaType.APPLICATION_JSON).build();
	}
	
	/**
	 * 
	 * @param orderId
	 * @return Json Data
	 * @throws Exception
	 */
	@GET
	@Path("/getView")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getView(@QueryParam("orderId") String orderId) {
	
		View outputMessage = null;
		
		if (orderId == null || orderId.trim().length() == 0) {
			return Response.serverError().entity("{\"status\":\"Order ID cannot be blank\"}")
					.build();
		}
		
		try {
			outputMessage = getDatabase().getView(orderId);
			logger.info(outputMessage);
			
		}
		catch (SQLException e) {
			logger.error(e);
			return Response
					.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("{\"status\":\"500 Internal Server Error; Reason: Server Out of Reach\"}")
					.build();
		}
		
		if (outputMessage == null) {
			
			return Response.status(Response.Status.NOT_FOUND)
					.entity("View Name not found for Order : " + orderId).build();
		}
		
		String outputJson = new JSONObject(outputMessage).toString();
		
		return Response.ok(outputJson, MediaType.APPLICATION_JSON).build();
		
	}
	
	/**
	 * 
	 * This service method is used for getting HistSeqID
	 * 
	 * @return Json Data
	 * @param orderId
	 * @throws Exception
	 */
	@GET
	@Path("/getHistSeqID")
	@Produces("application/json")
	public Response getHistSeqID(@QueryParam("orderId") String orderId,
			@QueryParam("task") String task) {
	
		HistSeqID hist = null;
		
		if (orderId == null || orderId.trim().length() == 0 || task == null
				|| task.trim().length() == 0) {
			return Response.serverError().entity("{\"status\":\"OrderID/Task cannot be blank\"}")
					.build();
		}
		
		try {
			
			hist = getDatabase().getHistSeqID(orderId, task);
			logger.info(hist);
		}
		
		catch (SQLException e) {
			logger.error(e);
			return Response
					.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("{\"status\":\"500 Internal Server Error; Reason: Server Out of Reach\"}")
					.build();
		}
		
		if (hist == null) {
			
			return Response.status(Response.Status.NOT_FOUND)
					.entity("Hist Sequence ID not found for Order : " + orderId).build();
		}
		
		String outputJson = new JSONObject(hist).toString();
		
		return Response.ok(outputJson, MediaType.APPLICATION_JSON).build();
		
	}
	
	/**
	 * 
	 * This service method is used for getting Catridges
	 * 
	 * @return Json Data
	 * @throws Exception
	 */
	@GET
	@Path("/getCatridges")
	@Produces("application/json")
	public Response getCatridges() {
	
		ArrayList<Catridges> catridges = null;
		
		try {
			
			catridges = (ArrayList<Catridges>) getDatabase().getCatridges();
			
			logger.info(catridges);
		}
		
		catch (SQLException e) {
			logger.error(e);
			return Response
					.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("{\"status\":\"500 Internal Server Error; Reason: Server Out of Reach\"}")
					.build();
		}
		
		String outputJson = new JSONArray(catridges).toString();
		
		return Response.ok(outputJson, MediaType.APPLICATION_JSON).build();
	}
	
	/**
	 * 
	 * This service method is used for getting Process Status
	 * 
	 * @return Json Data
	 * @throws Exception
	 */
	@GET
	@Path("/getProcessStatus")
	@Produces("application/json")
	// @Consumes("application/json")
	public Response getProcessStatus() {
	
		ArrayList<Process> process = null;
		
		try {
			
			process = (ArrayList<Process>) getDatabase().getProcessStatus();
			logger.info(process);
		}
		
		catch (SQLException e) {
			logger.error(e);
			return Response
					.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("{\"status\":\"500 Internal Server Error; Reason: Server Out of Reach\"}")
					.build();
		}
		
		String outputJson = new JSONArray(process).toString();
		
		return Response.ok(outputJson, MediaType.APPLICATION_JSON).build();
	}
	
	/**
	 * 
	 * This service method is used for getting Process of Order
	 * 
	 * @return Json Data
	 * @throws Exception
	 */
	@GET
	@Path("/getProcessofOrders")
	@Produces("application/json")
	public Response getProcessofOrders() {
	
		ArrayList<ProcessofOrders> processOrder = null;
		
		try {
			
			processOrder = (ArrayList<ProcessofOrders>) getDatabase().getProcessofOrders();
			
			logger.info(processOrder);
		}
		
		catch (SQLException e) {
			logger.error(e);
			return Response
					.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("{\"status\":\"500 Internal Server Error; Reason: Server Out of Reach\"}")
					.build();
		}
		
		String outputJson = new JSONArray(processOrder).toString();
		
		return Response.ok(outputJson, MediaType.APPLICATION_JSON).build();
	}
	
	/**
	 * 
	 * This service method is used for getting State of all Orders
	 * 
	 * @return Json Data
	 * 
	 */
	@GET
	@Path("/getAllOrderStates")
	@Produces("application/json")
	public Response getAllOrderStates() {
	
		ArrayList<AllOrderStates> orders = null;
		
		try {
			orders = (ArrayList<AllOrderStates>) getDatabase().getAllOrderStates();
			
			logger.info(orders);
		}
		
		catch (SQLException e) {
			logger.error(e);
			return Response
					.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("{\"status\":\"500 Internal Server Error; Reason: Server Out of Reach\"}")
					.build();
		}
		
		String outputJson = new JSONArray(orders).toString();
		
		return Response.ok(outputJson, MediaType.APPLICATION_JSON).build();
	}
	
	/**
	 * 
	 * This service method is used for getting all Processes
	 * 
	 * @return Json Data
	 * 
	 */
	@GET
	@Path("/getAllProcess")
	@Produces("application/json")
	public Response getAllProcess() {
	
		ArrayList<Process> process = null;
		
		try {
			process = (ArrayList<Process>) getDatabase().getAllProcess();
			
			logger.info(process);
		}
		
		catch (SQLException e) {
			logger.error(e);
			return Response
					.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("{\"status\":\"500 Internal Server Error; Reason: Server Out of Reach\"}")
					.build();
		}
		String outputJson = new JSONArray(process).toString();
		
		return Response.ok(outputJson, MediaType.APPLICATION_JSON).build();
	}
	
	/**
	 * 
	 * This service method is used for getting Process
	 * 
	 * @return Json Data
	 * 
	 */
	@GET
	@Path("/getProcess")
	@Produces("application/json")
	public Response getProcess(@QueryParam("orderId") String orderId,
			@QueryParam("task") String task) throws Exception {
	
		ArrayList<Status> process = null;
		
		if (orderId == null || orderId.trim().length() == 0) {
			return Response.serverError().entity("{\"status\":\"Order ID cannot be blank\"}")
					.build();
		}
		
		try {
			process = (ArrayList<Status>) getDatabase().getProcess(orderId, task);
			logger.info(process);
		}
		
		catch (SQLException e) {
			logger.error(e);
			return Response
					.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("{\"status\":\"500 Internal Server Error; Reason: Server Out of Reach\"}")
					.build();
		}
		
		String outputJson = new JSONArray(process).toString();
		
		return Response.ok(outputJson, MediaType.APPLICATION_JSON).build();
	}
	
	/**
	 * 
	 * This service method is used for getting Order State
	 * 
	 * @return Json Data
	 * 
	 */
	@GET
	@Path("/getOrderState")
	@Produces("application/json")
	public Response getOrderState() throws Exception {
	
		ArrayList<OrderStatus> process = null;
		
		try {
			process = (ArrayList<OrderStatus>) getDatabase().getOrderState();
			logger.info(process);
		}
		
		catch (SQLException e) {
			logger.error(e);
			return Response
					.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("{\"status\":\"500 Internal Server Error; Reason: Server Out of Reach\"}")
					.build();
		}
		
		String outputJson = new JSONArray(process).toString();
		logger.info(outputJson);
		return Response.ok(outputJson, MediaType.APPLICATION_JSON).build();
	}
	
	/**
	 * 
	 * This service method is used for getting Users
	 * 
	 * @return Json Data
	 * 
	 */
	@GET
	@Path("/getUsers")
	@Produces("application/json")
	public Response getUsers() {
	
		try {
			logger.info("In Get Users");
			return Response.ok(getDatabase().getUsers().toString(), MediaType.APPLICATION_JSON)
					.build();
			
		}
		catch (SQLException e) {
			logger.error(e);
			return Response
					.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("{\"status\":\"500 Internal Server Error; Reason: Server Out of Reach\"}")
					.build();
			
		}
	}
	
	/**
	 * 
	 * This service method is used for getting Workgroups
	 * 
	 * @return Json Data
	 * 
	 */
	@GET
	@Path("/getWorkgroups")
	@Produces("application/json")
	public Response getWorkgroups() {
	
		try {
			logger.info("In Get Workgroup");
			return Response
					.ok(getDatabase().getWorkgroups().toString(), MediaType.APPLICATION_JSON)
					.build();
		}
		catch (SQLException e) {
			
			logger.error(e);
			return Response
					.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("{\"status\":\"500 Internal Server Error; Reason: Server Out of Reach\"}")
					.build();
			
		}
	}
	
	/**
	 * 
	 * This service method is used for getting Task
	 * 
	 * @return Json Data
	 * 
	 */
	@GET
	@Path("/getTask")
	@Produces("application/json")
	public Response getTask() {
	
		try {
			logger.info("In Get Task");
			return Response.ok(getDatabase().getTask().toString(), MediaType.APPLICATION_JSON)
					.build();
		}
		catch (SQLException e) {
			logger.error(e);
			return Response
					.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("{\"status\":\"500 Internal Server Error; Reason: Server Out of Reach\"}")
					.build();
			
		}
	}
	
	/**
	 * 
	 * This service method is used for getting Source
	 * 
	 * @return Json Data
	 * 
	 */
	@GET
	@Path("/getSource")
	@Produces("application/json")
	public Response getSource() {
	
		try {
			
			logger.info("In Get Source");
			return Response.ok(getDatabase().getSource().toString(), MediaType.APPLICATION_JSON)
					.build();
		}
		catch (SQLException e) {
			logger.error(e);
			return Response
					.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("{\"status\":\"500 Internal Server Error; Reason: Server Out of Reach\"}")
					.build();
			
		}
	}
	
	/**
	 * 
	 * This service method is used for getting Type
	 * 
	 * @return Json Data
	 * 
	 */
	@GET
	@Path("/getType")
	@Produces("application/json")
	public Response getType() {
	
		try {
			logger.info("In Get Type");
			return Response.ok(getDatabase().getType().toString(), MediaType.APPLICATION_JSON)
					.build();
		}
		catch (SQLException e) {
			logger.error(e);
			return Response
					.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("{\"status\":\"500 Internal Server Error; Reason: Server Out of Reach\"}")
					.build();
			
		}
	}
	
	/**
	 * 
	 * This service method is used for getting Type
	 * 
	 * @return Json Data
	 * 
	 */
	@GET
	@Path("/reportForOrderState")
	@Produces("application/json")
	public Response reportForOrderState() {
	
		try {
			logger.info("reportForOrderState");
			return Response.ok(getDatabase().reportForOrderState().toString(),
					MediaType.APPLICATION_JSON).build();
		}
		catch (SQLException e) {
			logger.error(e);
			return Response
					.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("{\"status\":\"500 Internal Server Error; Reason: Server Out of Reach\"}")
					.build();
			
		}
	}
	
	/**
	 * 
	 * This service method is used for getting Type
	 * 
	 * @return Json Data
	 * 
	 */
	@GET
	@Path("/roleBasedPendingOrder")
	@Produces("application/json")
	public Response roleBasedPendingOrder() {
	
		try {
			logger.info("roleBasedPendingOrder");
			return Response.ok(getDatabase().roleBasedPendingOrder().toString(),
					MediaType.APPLICATION_JSON).build();
		}
		catch (SQLException e) {
			logger.error(e);
			return Response
					.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("{\"status\":\"500 Internal Server Error; Reason: Server Out of Reach\"}")
					.build();
			
		}
	}
	
}
