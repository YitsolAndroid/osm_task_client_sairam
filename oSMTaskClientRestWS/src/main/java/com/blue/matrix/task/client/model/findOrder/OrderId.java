
package com.blue.matrix.task.client.model.findOrder;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 
 * @author jaya sankar
 *
 */

@XmlRootElement(name = "find")
@XmlType(propOrder = { "orderId", "orderState", "targetOrderSatate", "reference", "cartridge",
		"orderType", "orderSource", "createdDate", "completeddate", "expectedStartDate",
		"expectedOrderCompletionDate", "requestedDeliveryDate", "expectedDuration", "priority",
		"field", "maxorders", "orderField" })
@XmlAccessorType(XmlAccessType.FIELD)
public class OrderId {
	
	@XmlElement(name = "OrderId")
	public String orderId;
	
	@XmlElement(name = "OrderState")
	public String orderState;
	
	@XmlElement(name = "TargetOrderState")
	public String targetOrderSatate;
	
	@XmlElement(name = "Reference")
	public String reference;
	
	@XmlElement(name = "Cartridge")
	public Cartridge cartridge;
	
	@XmlElement(name = "OrderType")
	public String orderType;
	
	@XmlElement(name = "OrderSource")
	public String orderSource;
	
	@XmlElement(name = "CreatedDate")
	public CreatedDate createdDate;
	
	@XmlElement(name = "CompletedDate")
	public CompletedDate completeddate;
	
	@XmlElement(name = "ExpectedStartDate")
	public ExpectedStartDate expectedStartDate;
	
	@XmlElement(name = "ExpectedOrderCompletionDate")
	public ExpectedOrderCompletionDate expectedOrderCompletionDate;
	
	@XmlElement(name = "RequestedDeliveryDate")
	public RequestedDeliveryDate requestedDeliveryDate;
	
	@XmlElement(name = "ExpectedDuration")
	public ExpectedDuration expectedDuration;
	
	@XmlElement(name = "Priority")
	public String priority;
	
	@XmlElement(name = "Field")
	public Field field;
	
	@XmlElement(name = "MaxOrders")
	public String maxorders;
	
	@XmlElement(name = "OrderField")
	public OrderField orderField;
	
}
