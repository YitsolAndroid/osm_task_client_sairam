
package com.blue.matrix.task.client.model.findOrder;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author jaya sankar
 *
 */

@XmlRootElement(namespace = "com.blue.matrix.task.client.model.OrderId")
public class Field {
	
	@XmlElement(name = "Cartridge")
	public Cartridge cartridge;
	@XmlElement(name = "Path")
	public String path;
	@XmlElement(name = "EqualTo")
	public String equalsto;
	@XmlElement(name = "From")
	public String from;
	@XmlElement(name = "To")
	public String to;
}