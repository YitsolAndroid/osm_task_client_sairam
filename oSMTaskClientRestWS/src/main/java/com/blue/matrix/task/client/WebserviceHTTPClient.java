
package com.blue.matrix.task.client;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;

import javax.servlet.ServletContext;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.blue.matrix.task.client.exception.GenericAllException;
import com.blue.matrix.task.client.model.TaskClientProperties;

/**
 * 
 * @author Jaya sankar
 *
 */
public class WebserviceHTTPClient {
	
	final static Logger logger = Logger.getLogger(WebserviceHTTPClient.class);
	public static TaskClientProperties properties = new TaskClientProperties();
	private static MessageFactory messageFactory;
	private static SOAPConnectionFactory factory;
	private static SOAPConnection connection;
	
	public WebserviceHTTPClient ( ) {
	
		try {
			factory = SOAPConnectionFactory.newInstance();
			connection = factory.createConnection();
			messageFactory = MessageFactory.newInstance();
			
		}
		catch (UnsupportedOperationException e) {
			
			logger.error(e);
			
		}
		catch (SOAPException e) {
			
			logger.error(e);
		}
		
	}
	
	/**
	 * This Method will add Security Header to Soap Message
	 * 
	 * @param message
	 * @param username
	 * @param password
	 * @throws SOAPException
	 * @throws Exception
	 */
	private void addWSSecurityHeader(SOAPMessage message, String username, String password)
			throws SOAPException {
	
		String temp = username + ":" + password;
		String authorization = new String(Base64.encodeBase64(temp.getBytes()));
		MimeHeaders hd = message.getMimeHeaders();
		hd.addHeader("Authorization", "Basic " + authorization);
		
		SOAPEnvelope envelope = message.getSOAPPart().getEnvelope();
		SOAPHeader header = message.getSOAPHeader();
		Name name = envelope.createName("Security", "wsse", properties.getWsse());
		SOAPElement security = header.addChildElement(name);
		name = envelope.createName("mustUnderstand", "soapenv", properties.getSoapenv());
		security.addAttribute(name, "1");
		
		name = envelope.createName("UsernameToken", "wsse", properties.getWsse());
		SOAPElement usernameToken = security.addChildElement(name);
		name = envelope.createName("Id", "wsu", properties.getWsu());
		usernameToken.addAttribute(name, "UsernameToken-4799946");
		
		name = envelope.createName("Username", "wsse", properties.getWsse());
		SOAPElement usernameElement = usernameToken.addChildElement(name);
		usernameElement.setTextContent(username);
		
		name = envelope.createName("Password", "wsse", properties.getWsse());
		SOAPElement passwordElement = usernameToken.addChildElement(name);
		name = envelope.createName("Type");
		passwordElement.addAttribute(name, properties.getPasswordtype());
		passwordElement.setTextContent(password);
		message.saveChanges();
	}
	
	/**
	 * This Method will create a soap call to Osm Server to Get Order
	 * 
	 * @param username
	 * @param password
	 * @param orderId
	 * @param ctx
	 * @return
	 * @throws SOAPException
	 * @throws MalformedURLException
	 * @throws FileNotFoundException
	 * @throws Exception
	 */
	public String getOrder(String username, String password, String orderId, String remark,
			ServletContext ctx) throws SOAPException {
	
		DB_Connection dbConnection = new DB_Connection();
		String viewName = dbConnection.getView(orderId).getViewName();
		logger.info("View name : " + viewName);
		
		SOAPMessage message = messageFactory.createMessage();
		message.getMimeHeaders().setHeader("SOAPAction", properties.getWs() + "/Orders");
		FileInputStream fis = null;
		try {
			
			fis = new FileInputStream(new File(ctx.getResource(properties.getGET_ORDER_FILE())
					.getPath()));
		}
		catch (FileNotFoundException | MalformedURLException e) {
			
			logger.error(e);
			throw new GenericAllException(
					"{\"errorCode\":401,\"errorMsg\":\"XML File   not found!\"}");
		}
		WebserviceHTTPClientUtil.setMessageContent(message, fis);
		addViewOrderId(orderId, viewName, message);
		message.getSOAPBody().getElementsByTagNameNS(properties.getWs(), "RetrieveRemarks").item(0)
				.setTextContent(remark);
		message.saveChanges();
		addWSSecurityHeader(message, username, password);
		String soapmessage = WebserviceHTTPClientUtil.soapCall(message, connection,
				properties.getEndpoint());
		
		return soapmessage;
		
	}
	
	private void addViewOrderId(String orderId, String viewName, SOAPMessage message)
			throws SOAPException {
	
		message.getSOAPBody().getElementsByTagNameNS(properties.getWs(), "OrderId").item(0)
				.setTextContent(orderId);
		message.getSOAPBody().getElementsByTagNameNS(properties.getWs(), "View").item(0)
				.setTextContent(viewName);
	}
	
	/**
	 * This Method will create a soap call to OSM Server to Get
	 * UpdateOrderUsingDataChange
	 * 
	 * @param username
	 * @param password
	 * @param orderId
	 * @param ctx
	 * @param startOrder
	 * @param dataChange
	 * @return
	 * @throws MalformedURLException
	 * @throws FileNotFoundException
	 * @throws SOAPException
	 * @throws Exception
	 */
	public String updateOrderUsingDataChange(String username, String password, String orderId,
			ServletContext ctx, String startOrder, String dataChange) throws SOAPException {
	
		SOAPMessage message = messageFactory.createMessage();
		message.getMimeHeaders().setHeader("SOAPAction", properties.getWs() + "/UpdateOrder");
		FileInputStream fis = null;
		try {
			
			fis = new FileInputStream(new File(ctx.getResource(properties.getDATA_CHANGE_FILE())
					.getPath()));
		}
		catch (FileNotFoundException | MalformedURLException e) {
			
			logger.error(e);
			throw new GenericAllException(
					"{\"errorCode\":401,\"errorMsg\":\"XML File   not found!\"}");
		}
		
		WebserviceHTTPClientUtil.setMessageContent(message, fis);
		
		String viewName = new DB_Connection().getView(orderId).getViewName();
		
		addViewOrderId(orderId, viewName, message);
		message.getSOAPBody().getElementsByTagNameNS(properties.getWs(), "DataChange").item(0)
				.setTextContent(dataChange);
		message.getSOAPBody().getElementsByTagNameNS(properties.getWs(), "StartOrder").item(0)
				.setTextContent(startOrder);
		message.saveChanges();
		
		addWSSecurityHeader(message, username, password);
		String soapmessage = WebserviceHTTPClientUtil.soapCall(message, connection,
				properties.getEndpoint());
		return soapmessage;
	}
	
	/**
	 * This Method will create a soap call to OSM Server to Get
	 * UpdateOrderUsingUpdatedNodes
	 * 
	 * @param username
	 * @param password
	 * @param orderId
	 * @param ctx
	 * @param payment
	 * @param payEntire
	 * @return
	 * @throws SOAPException
	 * @throws MalformedURLException
	 * @throws FileNotFoundException
	 * 
	 */
	public String updateOrderUsingUpdatedNodes(String username, String password, String orderId,
			ServletContext ctx, String payment, String payEntire) throws SOAPException {
	
		SOAPMessage message = messageFactory.createMessage();
		message.getMimeHeaders().setHeader("SOAPAction", properties.getWs() + "/UpdateOrder");
		FileInputStream fis = null;
		try {
			
			fis = new FileInputStream(new File(ctx.getResource(properties.getUPDATED_NODES_FILE())
					.getPath()));
		}
		catch (FileNotFoundException | MalformedURLException e) {
			
			logger.error(e);
			throw new GenericAllException(
					"{\"errorCode\":401,\"errorMsg\":\"XML File   not found!\"}");
		}
		WebserviceHTTPClientUtil.setMessageContent(message, fis);
		
		String viewName = new DB_Connection().getView(orderId).getViewName();
		
		addViewOrderId(orderId, viewName, message);
		message.getSOAPBody().getElementsByTagName("payment_method").item(0)
				.setTextContent(payment);
		message.getSOAPBody().getElementsByTagName("pay_entire_balance").item(0)
				.setTextContent(payEntire);
		message.saveChanges();
		
		addWSSecurityHeader(message, username, password);
		String soapmessage = WebserviceHTTPClientUtil.soapCall(message, connection,
				properties.getEndpoint());
		return soapmessage;
	}
	
	/**
	 * This Method will create a soap call to Osm Server to Get
	 * UpdateOrderUsingUpdatedOrder
	 * 
	 * @param username
	 * @param password
	 * @param orderId
	 * @param ctx
	 * @param amount
	 * @return
	 * @throws SOAPException
	 * @throws
	 * @throws IOException
	 * @throws SAXException
	 * @throws Exception
	 */
	public String updateOrderUsingUpdatedOrder(String username, String password, String orderId,
			ServletContext ctx, String xmlText) throws SOAPException {
	
		SOAPMessage message = messageFactory.createMessage();
		message.getMimeHeaders().setHeader("SOAPAction", properties.getWs() + "/UpdateOrder");
		FileInputStream fis = null;
		try {
			
			fis = new FileInputStream(new File(ctx.getResource(properties.getUPDATED_ORDER_FILE())
					.getPath()));
		}
		catch (FileNotFoundException | MalformedURLException e) {
			
			logger.error(e);
			throw new GenericAllException(
					"{\"errorCode\":401,\"errorMsg\":\"XML File   not found!\"}");
			
		}
		
		DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
		builderFactory.setNamespaceAware(true);
		InputStream stream = new ByteArrayInputStream(xmlText.getBytes());
		Document doc = null;
		try {
			
			doc = builderFactory.newDocumentBuilder().parse(stream);
		}
		catch (SAXException | ParserConfigurationException | IOException e) {
			
			logger.error(e);
			throw new GenericAllException(
					"{\"errorCode\":401,\"errorMsg\":\"XML File   not found!\"}");
			
		}
		
		WebserviceHTTPClientUtil.setMessageContent(message, fis);
		String viewName = new DB_Connection().getView(orderId).getViewName();
		addViewOrderId(orderId, viewName, message);
		SOAPBodyElement updatedOrder = message.getSOAPBody().addDocument(doc);
		SOAPElement updatedOrderElement = (SOAPElement) message.getSOAPBody()
				.getElementsByTagNameNS(properties.getWs(), "UpdatedOrder").item(0);
		updatedOrderElement.addChildElement(updatedOrder);
		message.saveChanges();
		
		addWSSecurityHeader(message, username, password);
		
		String soapmessage = WebserviceHTTPClientUtil.soapCall(message, connection,
				properties.getEndpoint());
		return soapmessage;
	}
	
	/**
	 * This Method will create a soap call to Osm Server to perform Resume Order
	 * 
	 * @param username
	 * @param password
	 * @param orderId
	 * @param reason
	 * @param ctx
	 * @return
	 * @throws SOAPException
	 * @throws Exception
	 */
	public String resumeOrder(String username, String password, String orderId, String reason,
			ServletContext ctx) throws SOAPException {
	
		SOAPMessage message = messageFactory.createMessage();
		message.getMimeHeaders().setHeader("SOAPAction", properties.getWs() + "/ResumeOrder");
		FileInputStream fis = null;
		try {
			
			File file = new File(ctx.getResource(properties.getRESUME_ORDER()).getPath());
			fis = new FileInputStream(file);
		}
		catch (FileNotFoundException | MalformedURLException e) {
			
			logger.error(e);
			throw new GenericAllException(
					"{\"errorCode\":401,\"errorMsg\":\"XML File   not found!\"}");
			
		}
		WebserviceHTTPClientUtil.setMessageContent(message, fis);
		
		addingOrderId(orderId, reason, message);
		message.saveChanges();
		
		addWSSecurityHeader(message, username, password);
		String soapmessage = WebserviceHTTPClientUtil.soapCall(message, connection,
				properties.getEndpoint());
		return soapmessage;
		
	}
	
	/**
	 * This Method will create a soap call to Osm Server to perform Suspend
	 * Order
	 * 
	 * @param username
	 * @param password
	 * @param orderId
	 * @param reason
	 * @param ctx
	 * @return
	 * @throws SOAPException
	 * @throws Exception
	 */
	public String suspendOrder(String username, String password, String orderId, String reason,
			ServletContext ctx) throws SOAPException {
	
		SOAPMessage message = messageFactory.createMessage();
		message.getMimeHeaders().setHeader("SOAPAction", properties.getWs() + "/SuspendOrder");
		
		FileInputStream fis = null;
		try {
			
			fis = new FileInputStream(new File(ctx.getResource(properties.getSUSPEND_ORDER())
					.getPath()));
		}
		catch (FileNotFoundException | MalformedURLException e) {
			
			logger.error(e);
			throw new GenericAllException(
					"{\"errorCode\":401,\"errorMsg\":\"XML File   not found!\"}");
			
		}
		WebserviceHTTPClientUtil.setMessageContent(message, fis);
		
		addingOrderId(orderId, reason, message);
		message.saveChanges();
		
		addWSSecurityHeader(message, username, password);
		String soapmessage = WebserviceHTTPClientUtil.soapCall(message, connection,
				properties.getEndpoint());
		return soapmessage;
	}
	
	private void addingOrderId(String orderId, String reason, SOAPMessage message)
			throws SOAPException {
	
		message.getSOAPBody().getElementsByTagNameNS(properties.getWs(), "OrderId").item(0)
				.setTextContent(orderId);
		message.getSOAPBody().getElementsByTagNameNS(properties.getWs(), "Reason").item(0)
				.setTextContent(reason);
	}
	
	/**
	 * This Method will create a soap call to Osm Server to Cancel order
	 * 
	 * @param username
	 * @param password
	 * @param orderId
	 * @param ctx
	 * @return
	 * @throws SOAPException
	 * @throws Exception
	 */
	
	public String cancelOrder(String username, String password, String orderId, String reason,
			ServletContext ctx) throws SOAPException {
	
		SOAPMessage message = messageFactory.createMessage();
		message.getMimeHeaders().setHeader("SOAPAction", properties.getWs() + "/CancelOrder");
		
		FileInputStream fis = null;
		try {
			
			fis = new FileInputStream(new File(ctx.getResource(properties.getCANCEL_ORDER())
					.getPath()));
		}
		catch (FileNotFoundException | MalformedURLException e) {
			
			logger.error(e);
		}
		WebserviceHTTPClientUtil.setMessageContent(message, fis);
		
		addingOrderId(orderId, reason, message);
		message.saveChanges();
		
		addWSSecurityHeader(message, username, password);
		String soapmessage = WebserviceHTTPClientUtil.soapCall(message, connection,
				properties.getEndpoint());
		return soapmessage;
		
	}
	
	/**
	 * This Method will create a soap call to Osm Server to perform Fail Order
	 * 
	 * @param username
	 * @param password
	 * @param orderId
	 * @param ctx
	 * @return
	 * @throws SOAPException
	 * @throws Exception
	 */
	public String failOrder(String username, String password, String orderId, ServletContext ctx)
			throws SOAPException {
	
		SOAPMessage message = messageFactory.createMessage();
		message.getMimeHeaders().setHeader("SOAPAction", properties.getWs() + "/FailOrder");
		
		FileInputStream fis = null;
		try {
			
			fis = new FileInputStream(new File(ctx.getResource(properties.getFAIL_ORDER())
					.getPath()));
			
		}
		catch (FileNotFoundException | MalformedURLException e) {
			
			logger.error(e);
			throw new GenericAllException(
					"{\"errorCode\":401,\"errorMsg\":\"XML File   not found!\"}");
			
		}
		WebserviceHTTPClientUtil.setMessageContent(message, fis);
		
		message.getSOAPBody().getElementsByTagNameNS(properties.getWs(), "OrderId").item(0)
				.setTextContent(orderId);
		message.saveChanges();
		
		addWSSecurityHeader(message, username, password);
		String soapmessage = WebserviceHTTPClientUtil.soapCall(message, connection,
				properties.getEndpoint());
		return soapmessage;
		
	}
	
	/**
	 * This Method will create a soap call to Osm Server to Get Abort Order
	 * 
	 * @param username
	 * @param password
	 * @param orderId
	 * @param reason
	 * @param ctx
	 * @return
	 * @throws SOAPException
	 * @throws Exception
	 */
	public String abortOrder(String username, String password, String orderId, String reason,
			ServletContext ctx) throws SOAPException {
	
		SOAPMessage message = messageFactory.createMessage();
		message.getMimeHeaders().setHeader("SOAPAction", properties.getWs() + "/AbortOrder");
		
		FileInputStream fis = null;
		try {
			
			fis = new FileInputStream(new File(ctx.getResource(properties.getABORT_ORDER())
					.getPath()));
			
		}
		catch (FileNotFoundException | MalformedURLException e) {
			
			logger.error(e);
			throw new GenericAllException(
					"{\"errorCode\":401,\"errorMsg\":\"XML File   not found!\"}");
			
		}
		WebserviceHTTPClientUtil.setMessageContent(message, fis);
		addingOrderId(orderId, reason, message);
		message.saveChanges();
		
		addWSSecurityHeader(message, username, password);
		String soapmessage = WebserviceHTTPClientUtil.soapCall(message, connection,
				properties.getEndpoint());
		return soapmessage;
		
	}
	
	/**
	 * This Method will create a soap call to Osm Server to perform
	 * ResolveFailure
	 * 
	 * @param username
	 * @param password
	 * @param orderId
	 * @param ctx
	 * @return
	 * @throws MalformedURLException
	 * @throws FileNotFoundException
	 * @throws SOAPException
	 * @throws Exception
	 */
	public String resolveFailure(String username, String password, String orderId, String reason,
			ServletContext ctx) throws SOAPException {
	
		SOAPMessage message = messageFactory.createMessage();
		message.getMimeHeaders().setHeader("SOAPAction", properties.getWs() + "/ResolveFailure");
		FileInputStream fis;
		try {
			
			fis = new FileInputStream(new File(ctx.getResource(properties.getRESOLVE_ORDER())
					.getPath()));
			
		}
		catch (FileNotFoundException | MalformedURLException e) {
			
			logger.error(e);
			throw new GenericAllException(
					"{\"errorCode\":401,\"errorMsg\":\"Xml File is  not found!\"}");
			
		}
		WebserviceHTTPClientUtil.setMessageContent(message, fis);
		addingOrderId(orderId, reason, message);
		message.saveChanges();
		addWSSecurityHeader(message, username, password);
		
		String soapmessage = WebserviceHTTPClientUtil.soapCall(message, connection,
				properties.getEndpoint());
		return soapmessage;
		
	}
	
}
