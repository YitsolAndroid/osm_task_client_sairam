
package com.blue.matrix.task.client.restws.dynamicsql;

import com.blue.matrix.task.client.dynamicsql.GroupBy;
import com.blue.matrix.task.client.dynamicsql.Table;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import org.json.*;

public class GroupListData {
	
	private List groupList;
	private JSONArray jsongroupList;
	
	public GroupListData ( ) {
	
		jsongroupList = null;
	}
	
	List setGroupList(JSONArray jsongroupList) {
	
		if (jsongroupList != null) {
			try {
				groupList = new ArrayList();
				for (int i = 0; i < jsongroupList.length(); i++) {
					JSONObject jo = new JSONObject(jsongroupList.get(i).toString());
					GroupBy gb = new GroupBy();
					for (int j = 0; j < jsongroupList.length(); j++) {
						JSONObject jt1 = (JSONObject) jo.get("table1");
						String name1 = jt1.getString("name");
						String alias1 = jt1.getString("alias");
						Table tb1 = new Table(name1, alias1);
						String s = jo.get("colum").toString();
						gb.setColum(s);
						gb.setTable1(tb1);
					}
					
					groupList.add(gb);
				}
				
			}
			catch (JSONException e) {
				
				e.printStackTrace();
			}
			catch (Exception e) {
				
				e.printStackTrace();
			}
		}
		return groupList;
	}
}
