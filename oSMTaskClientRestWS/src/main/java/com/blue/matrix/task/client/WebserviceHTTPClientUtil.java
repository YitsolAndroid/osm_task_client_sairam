
package com.blue.matrix.task.client;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URL;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFault;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.blue.matrix.task.client.exception.GenericException;
import com.blue.matrix.task.client.model.TaskClientProperties;
import com.blue.matrix.task.client.model.findOrder.OrderId;

/**
 * 
 * @author Jaya sankar
 *
 */
public class WebserviceHTTPClientUtil {
	
	final static Logger logger = Logger.getLogger(WebserviceHTTPClient.class);
	
	TaskClientProperties properties = new TaskClientProperties();
	
	private static Document JAXBElementToDomElement(OrderId element) {
	
		try {
			
			JAXBContext jc = JAXBContext.newInstance(new Class[] { OrderId.class });
			Marshaller umarshal = jc.createMarshaller();
			StringWriter sw = new StringWriter();
			umarshal.marshal(element, sw);
			
			InputStream inputStream = new ByteArrayInputStream(sw.toString().getBytes());
			Document xmlDocument = DocumentBuilderFactory.newInstance().newDocumentBuilder()
					.parse(inputStream);
			return xmlDocument;
		}
		catch (Exception ex) {
			
			logger.error(ex);
		}
		return null;
		
	}
	
	/**
	 * This method will add File Data into Soap message
	 * 
	 * @param message
	 * @param file
	 * @throws IOException
	 * @throws SAXException
	 * @throws SOAPException
	 * @throws Exception
	 */
	public static void setMessageContent(SOAPMessage message, FileInputStream file)
			throws SOAPException {
	
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		dbFactory.setNamespaceAware(true);
		DocumentBuilder dBuilder = null;
		
		try {
			
			dBuilder = dbFactory.newDocumentBuilder();
			message.getSOAPPart().setContent(new DOMSource(dBuilder.parse(file)));
			
		}
		catch (ParserConfigurationException | SAXException | IOException e) {
			
			logger.error(e);
			
		}
	}
	
	public static String soapCall(SOAPMessage message, SOAPConnection connection, URL endpoint)
			throws SOAPException {
	
		StringWriter stringResult = new StringWriter();
		
		try {
			
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			message.writeTo(out);
			SOAPMessage response = connection.call(message, endpoint);
			SOAPFault fault = response.getSOAPBody().getFault();
			
			if (fault != null) {
				
				logger.info(fault.getFaultString());
				
			}
			ByteArrayOutputStream resp = new ByteArrayOutputStream();
			response.writeTo(resp);
			DOMSource source = new DOMSource(response.getSOAPBody());
			
			TransformerFactory.newInstance().newTransformer()
					.transform(source, new StreamResult(stringResult));
			
		}
		catch (TransformerException | TransformerFactoryConfigurationError | IOException e) {
			
			logger.error(e);
			
		}
		
		String soapmessage = stringResult.toString();
		return soapmessage;
	}
	
}
