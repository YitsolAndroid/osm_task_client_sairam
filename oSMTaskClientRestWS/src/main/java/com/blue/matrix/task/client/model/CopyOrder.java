
package com.blue.matrix.task.client.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CopyOrder {
	
	@XmlElement(name = "OriginalOrderID")
	private String originalOrderId;
	@XmlElement(name = "OrderSource")
	private String orderSources;
	@XmlElement(name = "OrderType")
	private String orderType;
	@XmlElement(name = "Reference")
	private String reference;
	@XmlElement(name = "Priority")
	private String priority;
	@XmlElement(name = "Namespace")
	private String nameSpace;
	@XmlElement(name = "Version")
	private String version;
	
	public String getOriginalOrderId() {
	
		return originalOrderId;
	}
	
	public String getOrderSources() {
	
		return orderSources;
	}
	
	public String getOrderType() {
	
		return orderType;
	}
	
	public String getReference() {
	
		return reference;
	}
	
	public String getPriority() {
	
		return priority;
	}
	
	public String getNameSpace() {
	
		return nameSpace;
	}
	
	public String getVersion() {
	
		return version;
	}
	
	public void setOriginalOrderId(String originalOrderId) {
	
		this.originalOrderId = originalOrderId;
	}
	
	public void setOrderSources(String orderSources) {
	
		this.orderSources = orderSources;
	}
	
	public void setOrderType(String orderType) {
	
		this.orderType = orderType;
	}
	
	public void setReference(String reference) {
	
		this.reference = reference;
	}
	
	public void setPriority(String priority) {
	
		this.priority = priority;
	}
	
	public void setNameSpace(String nameSpace) {
	
		this.nameSpace = nameSpace;
	}
	
	public void setVersion(String version) {
	
		this.version = version;
	}
	
}
