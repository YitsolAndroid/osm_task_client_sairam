
package com.blue.matrix.task.client.model.createorder;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CreateOrder {
	
	@XmlElement(name = "OrderSource")
	private String orderSource;
	
	@XmlElement(name = "OrderType")
	private String orderType;
	
	@XmlElement(name = "Reference")
	private String reference;
	
	@XmlElement(name = "Priority")
	private String priority;
	
	@XmlElement(name = "Namespace")
	private String namespace;
	
	@XmlElement(name = "Version")
	private String version;
	
	@XmlElement(name = "_root")
	private ClientInfo clientInfo;
	
	public void setOrderSource(String orderSource) {
	
		this.orderSource = orderSource;
	}
	
	public void setOrderType(String orderType) {
	
		this.orderType = orderType;
	}
	
	public void setReference(String reference) {
	
		this.reference = reference;
	}
	
	public void setPriority(String priority) {
	
		this.priority = priority;
	}
	
	public void setNamespace(String namespace) {
	
		this.namespace = namespace;
	}
	
	public void setVersion(String version) {
	
		this.version = version;
	}
	
	public void setClientInfo(ClientInfo clientInfo) {
	
		this.clientInfo = clientInfo;
	}
	
}
