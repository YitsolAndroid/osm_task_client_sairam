
package com.blue.matrix.task.client.model;

/**
 * 
 * @author jaya sankar
 *
 */

public class ProcessofOrders {
	
	private String processIdDescription;
	private String orderSeqId;
	
	public String getProcessIdDescription() {
	
		return processIdDescription;
	}
	
	public void setProcessIdDescription(String processIdDescription) {
	
		this.processIdDescription = processIdDescription;
	}
	
	public String getOrderSeqId() {
	
		return orderSeqId;
	}
	
	public void setOrderSeqId(String orderSeqId) {
	
		this.orderSeqId = orderSeqId;
	}
	
}
