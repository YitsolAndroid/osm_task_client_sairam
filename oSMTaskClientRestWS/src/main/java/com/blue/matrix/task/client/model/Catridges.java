
package com.blue.matrix.task.client.model;

/**
 * 
 * @author jaya sankar
 *
 */

public class Catridges {
	
	private String namespace;
	private String version;
	
	public String getNamespace() {
	
		return namespace;
	}
	
	public void setNamespace(String namespace) {
	
		this.namespace = namespace;
	}
	
	public String getVersion() {
	
		return version;
	}
	
	public void setVersion(String version) {
	
		this.version = version;
	}
	
}
