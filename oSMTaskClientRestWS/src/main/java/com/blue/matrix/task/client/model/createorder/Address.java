
package com.blue.matrix.task.client.model.createorder;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(namespace = "com.blue.matrix.task.client.model.createorder.CreateOrder")
public class Address {
	
	@XmlElement(name = "street1")
	private String street1;
	@XmlElement(name = "city")
	private String city;
	@XmlElement(name = "state")
	private String state;
	@XmlElement(name = "country")
	private String country;
	@XmlElement(name = "zip")
	private String zip;
	
}
