
package com.blue.matrix.task.client.model.findOrder;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author jaya sankar
 *
 */

@XmlRootElement(namespace = "com.blue.matrix.task.client.model.OrderId")
public class ExpectedStartDate {
	
	@XmlElement(name = "FromDate")
	public String fromDate;
	@XmlElement(name = "ToDate")
	public String toDate;
	
}
