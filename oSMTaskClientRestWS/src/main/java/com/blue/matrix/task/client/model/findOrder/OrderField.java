
package com.blue.matrix.task.client.model.findOrder;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author jaya sankar
 *
 */
@XmlRootElement(namespace = "com.blue.matrix.task.client.model.OrderId")
public class OrderField {
	
	@XmlElement(name = "SortBy")
	public String sortBy;
	@XmlElement(name = "Header")
	public String header;
	
}
