
package com.blue.matrix.task.client.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author jaya sankar
 *
 */

@XmlRootElement
public class GetTaskStatusesRequest {
	
	@XmlElement(name = "Task")
	private String task;
	
	@XmlElement(name = "Namespace")
	private String namePsace;
	
	@XmlElement(name = "Version")
	private String version;
	
	public void setTask(String task) {
	
		this.task = task;
	}
	
	public void setNamePsace(String namePsace) {
	
		this.namePsace = namePsace;
	}
	
	public void setVersion(String version) {
	
		this.version = version;
	}
	
}
