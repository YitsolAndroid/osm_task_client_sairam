/*
* Copyright (C) 2015, 2016, Blue Matrix and/or its affiliates. All rights reserved. 
 */
package com.bm.task.client.utils;

public interface EditNodeListner 
{
	public void getEditNodeText(String editNode);

}
