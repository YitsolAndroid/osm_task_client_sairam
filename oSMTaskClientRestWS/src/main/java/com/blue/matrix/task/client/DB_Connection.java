
package com.blue.matrix.task.client;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.sql.PooledConnection;

import oracle.jdbc.pool.OracleConnectionPoolDataSource;
import oracle.jdbc.pool.OracleDataSource;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.blue.matrix.task.client.dynamicsql.Data;
import com.blue.matrix.task.client.dynamicsql.GroupBy;
import com.blue.matrix.task.client.dynamicsql.Join;
import com.blue.matrix.task.client.dynamicsql.SearchCriteria;
import com.blue.matrix.task.client.dynamicsql.SelectQuery;
import com.blue.matrix.task.client.dynamicsql.Table;
import com.blue.matrix.task.client.exception.GenericAllException;
import com.blue.matrix.task.client.exception.GenericException;
import com.blue.matrix.task.client.model.AllOrderStates;
import com.blue.matrix.task.client.model.Catridges;
import com.blue.matrix.task.client.model.HistSeqID;
import com.blue.matrix.task.client.model.OrderStatus;
import com.blue.matrix.task.client.model.Process;
import com.blue.matrix.task.client.model.ProcessofOrders;
import com.blue.matrix.task.client.model.Status;
import com.blue.matrix.task.client.model.View;

/**
 * 
 * @author Jay sankar
 * 
 * @Descrption This class is use for executing Database queries
 *
 */
public class DB_Connection {
	
	private static final String PROCESS_STATUS_ID = "process_status_id";
	private static final String PROCESS_ID2 = "process_id";
	final static Logger logger = Logger.getLogger(DB_Connection.class);
	
	static Connection getConnection() {
	
		DBConnection connection = DBConnection.getDBConnectionInstance();
		try {
			
			Class.forName(connection.getDRIVER_NAME());
			Connection con = DriverManager.getConnection(connection.getCONNECTION_URL(),
					connection.getDB_USER_NAME(), connection.getDB_PASSWORD());
			return con;
			
		}
		catch (SQLException | ClassNotFoundException e) {
			
			logger.error(e);
			throw new GenericAllException(
					"{\"errorCode\":401,\"errorMsg\":\"DataBase Connection  not found!\"}"); //$NON-NLS-1$
			
		}
	}
	
	/**
	 * @Descrption This method will return List of Catridges Objects
	 * @return
	 * @throws SQLException
	 */
	public List<Catridges> getCatridges() {
	
		logger.info("-----------------Getting Catridges------------------------"); //$NON-NLS-1$
		List<Catridges> catridges = new ArrayList<Catridges>();
		try {
			
			final Connection con = getConnection();
			final String sql = "select NAMESPACE_MNEMONIC,VERSION from om_cartridge"; //$NON-NLS-1$
			final PreparedStatement prepareStmt = con.prepareStatement(sql);
			final ResultSet rs = prepareStmt.executeQuery(sql);
			while (rs.next()) {
				
				String nameSpace = rs.getString("NAMESPACE_MNEMONIC"); //$NON-NLS-1$
				String version = rs.getString("VERSION"); //$NON-NLS-1$
				logger.info("Catridges :--------nameSpace : " + nameSpace //$NON-NLS-1$
						+ ", version :" + version); //$NON-NLS-1$
				Catridges cad = new Catridges();
				cad.setNamespace(nameSpace);
				cad.setVersion(version);
				catridges.add(cad);
				
			}
			rs.close();
			prepareStmt.close();
			
		}
		catch (SQLException e) {
			
			logger.error(e);
			
			throw new GenericAllException(
					"{\"errorCode\":401,\"errorMsg\":\"DataBase Connection  not found!\"}"); //$NON-NLS-1$
			
		}
		return catridges;
	}
	
	/**
	 * @Descrption This method will return List of ProcessofOrders Class Objects
	 * @return
	 * @throws SQLException
	 */
	public List<ProcessofOrders> getProcessofOrders() {
	
		logger.info("-----------------Getting ProcessofOrders------------------------"); //$NON-NLS-1$
		List<ProcessofOrders> processOrder = new ArrayList<ProcessofOrders>();
		try {
			
			final Connection con = getConnection();
			final String sql = "SELECT process_id_description,order_seq_id FROM om_process INNER JOIN om_order_flow ON om_process.process_id = om_order_flow.process_id"; //$NON-NLS-1$
			final PreparedStatement prepareStmt = con.prepareStatement(sql);
			final ResultSet rs = prepareStmt.executeQuery();
			while (rs.next()) {
				
				String processId = rs.getString("process_id_description"); //$NON-NLS-1$
				String orderSeqId = rs.getString("order_seq_id"); //$NON-NLS-1$
				logger.info("ProcessofOrders:--------------nameSpace : " //$NON-NLS-1$
						+ processId + ", version :" + orderSeqId); //$NON-NLS-1$
				
				ProcessofOrders mapData = new ProcessofOrders();
				mapData.setProcessIdDescription(processId);
				mapData.setOrderSeqId(orderSeqId);
				processOrder.add(mapData);
			}
			
			rs.close();
			prepareStmt.close();
			con.close();
			
		}
		catch (SQLException e) {
			
			logger.error(e);
			throw new GenericAllException(
					"{\"errorCode\":401,\"errorMsg\":\"DataBase Connection  not found!\"}"); //$NON-NLS-1$
			
		}
		
		return processOrder;
	}
	
	/**
	 * @Descrption This method will take OrderSeqId String as input will return
	 *             HistSeqId Class Object
	 * @param order_seq_id
	 * @return
	 * @throws SQLException
	 */
	public HistSeqID getHistSeqID(String order_seq_id, String task) {
	
		logger.info("-----------------Getting getHistSeqID------------------------"); //$NON-NLS-1$
		HistSeqID hist = new HistSeqID();
		
		try {
			
			final Connection con = getConnection();
			String HIST_SEQ_ID = null;
			final String sql = "select om_order_flow.hist_seq_id as HIST_SEQ_ID from om_order_flow " //$NON-NLS-1$
					+ "inner join om_task on om_order_flow.task_id = om_task.task_id " //$NON-NLS-1$
					+ "where om_order_flow.order_seq_id=? and om_task.task_mnemonic=?"; //$NON-NLS-1$
			PreparedStatement prepareStmt = con.prepareStatement(sql);
			prepareStmt.setInt(1, Integer.parseInt(order_seq_id));
			prepareStmt.setString(2, task);
			ResultSet rs = prepareStmt.executeQuery();
			while (rs.next()) {
				
				HIST_SEQ_ID = rs.getString("HIST_SEQ_ID"); //$NON-NLS-1$
				hist.setHistSeqId(HIST_SEQ_ID);
				logger.info("-----getHistSeqID--" + HIST_SEQ_ID); //$NON-NLS-1$
				
			}
			
			con.close();
			
		}
		catch (NumberFormatException e) {
			
			logger.error(e);
			throw new GenericAllException(
					"{\"errorCode\":401,\"errorMsg\":\"HistSeqID  is null \"}"); //$NON-NLS-1$
			
		}
		catch (SQLException e) {
			
			logger.error(e);
			throw new GenericAllException(
					"{\"errorCode\":401,\"errorMsg\":\"DataBase Connection  not found!\"}"); //$NON-NLS-1$
			
		}
		
		return hist;
	}
	
	/**
	 * @Descrption This Method List of OrderStates Class Objects
	 * @return
	 * @throws SQLException
	 */
	public List<AllOrderStates> getAllOrderStates() {
	
		final List<AllOrderStates> orders = new ArrayList<AllOrderStates>();
		try {
			
			final Connection con = getConnection();
			final String sql = "select state_id ,state_description from om_state"; //$NON-NLS-1$
			final PreparedStatement prepareStmt = con.prepareStatement(sql);
			ResultSet rs = prepareStmt.executeQuery(sql);
			while (rs.next()) {
				
				String state_id = rs.getString("state_id"); //$NON-NLS-1$
				String state_description = rs.getString("state_description"); //$NON-NLS-1$
				logger.info("nameSpace : " + state_id + ", state_description :" //$NON-NLS-1$ //$NON-NLS-2$
						+ state_description);
				
				AllOrderStates mapData = new AllOrderStates();
				mapData.setStateId(state_id);
				mapData.setStatedesc(state_description);
				orders.add(mapData);
			}
			
			rs.close();
			prepareStmt.close();
			con.close();
			
		}
		catch (SQLException e) {
			
			logger.error(e);
			throw new GenericAllException(
					"{\"errorCode\":401,\"errorMsg\":\"DataBase Connection  not found!\"}"); //$NON-NLS-1$
			
		}
		
		return orders;
		
	}
	
	/**
	 * @Descrption This Method will return List of Process Class Objects
	 * @return
	 * @throws SQLException
	 */
	
	public List<Process> getAllProcess() {
	
		List<Process> process = new ArrayList<Process>();
		try {
			
			final Connection con = getConnection();
			final String sql = "SELECT process_id,process_id_description  from OM_PROCESS"; //$NON-NLS-1$
			final PreparedStatement prepareStmt = con.prepareStatement(sql);
			final ResultSet rs = prepareStmt.executeQuery();
			while (rs.next()) {
				
				String process_id = rs.getString(PROCESS_ID2); //$NON-NLS-1$
				String process_id_description = rs.getString("process_id_description"); //$NON-NLS-1$
				logger.info(Messages.getString("DB_Connection.37") + process_id //$NON-NLS-1$
						+ ", process_id_description :" + process_id_description); //$NON-NLS-1$
				
				Process mapData = new Process();
				mapData.setProcessId(process_id);
				mapData.setProcessIdDesc(process_id_description);
				process.add(mapData);
			}
			rs.close();
			prepareStmt.close();
			con.close();
			
		}
		catch (SQLException e) {
			
			logger.error(e);
			throw new GenericAllException(
					"{\"errorCode\":401,\"errorMsg\":\"DataBase Connection  not found!\"}"); //$NON-NLS-1$
			
		}
		
		return process;
		
	}
	
	/**
	 * @Descrption This method will take OrderId as input and will return
	 *             approximate View Class Object
	 * @param orderId
	 * @return
	 * @throws GenericAllException
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public View getView(String orderId) {
	
		String view = null;
		View viewObject = new View();
		Statement stmnt;
		try {
			
			final Connection con = getConnection();
			stmnt = con.createStatement();
			final String sql = "select TASK_MNEMONIC from om_order_header " //$NON-NLS-1$
					+ "inner join om_workgroup_order_view on om_workgroup_order_view.order_source_id=om_order_header.order_source_id " //$NON-NLS-1$
					+ "inner join om_task_order_view on om_task_order_view.order_view_seq_id=om_workgroup_order_view.order_view_seq_id " //$NON-NLS-1$
					+ "inner join om_task on om_task.task_id=om_task_order_view.task_id where order_seq_id=" //$NON-NLS-1$
					+ orderId;
			ResultSet rs = stmnt.executeQuery(sql);
			while (rs.next()) {
				view = rs.getString("TASK_MNEMONIC"); //$NON-NLS-1$
				
			}
			
			logger.info("View Name :" + view); //$NON-NLS-1$
			con.close();
			viewObject.setViewName(view);
		}
		catch (SQLException e) {
			
			logger.error(e);
			throw new GenericAllException(
					"{\"errorCode\":401,\"errorMsg\":\"DataBase Connection  not found!\"}"); //$NON-NLS-1$
			
		}
		return viewObject;
	}
	
	/**
	 * @Descrption This Method will return List of Process Class Objects
	 * @return
	 * @throws SQLException
	 */
	
	public List<Process> getProcessStatus() {
	
		List<Process> process = new ArrayList<Process>();
		final String sql = "select process_status_id,process_status_description from om_process_status"; //$NON-NLS-1$
		ResultSet rs;
		try {
			
			rs = getConnection().prepareStatement(sql).executeQuery();
			while (rs.next()) {
				
				String process_id = rs.getString(PROCESS_STATUS_ID); //$NON-NLS-1$
				String process_id_description = rs.getString("process_status_description"); //$NON-NLS-1$
				logger.info("nameSpace : " + process_id //$NON-NLS-1$
						+ ", process_id_description :" + process_id_description); //$NON-NLS-1$
				
				Process mapData = new Process();
				mapData.setProcessId(process_id);
				mapData.setProcessIdDesc(process_id_description);
				process.add(mapData);
			}
			
			rs.close();
		}
		catch (SQLException e) {
			
			logger.error(e);
			throw new GenericAllException(
					"{\"errorCode\":401,\"errorMsg\":\"DataBase Connection  not found!\"}"); //$NON-NLS-1$
			
		}
		
		return process;
		
	}
	
	/**
	 * @Descrption This Method will return List of Process Class Objects
	 * @return
	 * @throws SQLException
	 */
	
	public List<Process> getProcessStatusMnemonic() {
	
		List<Process> process = new ArrayList<Process>();
		final String sql = "select process_status_id,process_status_description from om_process_status"; //$NON-NLS-1$
		
		ResultSet rs;
		try {
			
			rs = getConnection().prepareStatement(sql).executeQuery(sql);
			while (rs.next()) {
				
				String process_id = rs.getString(PROCESS_STATUS_ID); //$NON-NLS-1$
				String process_id_description = rs.getString("process_status_description"); //$NON-NLS-1$
				logger.info("nameSpace : " + process_id //$NON-NLS-1$
						+ ", process_id_description :" + process_id_description); //$NON-NLS-1$
				
				Process mapData = new Process();
				mapData.setProcessId(process_id);
				mapData.setProcessIdDesc(process_id_description);
				process.add(mapData);
			}
		}
		catch (SQLException e) {
			
			logger.error(e);
			throw new GenericAllException(
					"{\"errorCode\":401,\"errorMsg\":\"DataBase Connection  not found!\"}"); //$NON-NLS-1$
			
		}
		
		return process;
		
	}
	
	/**
	 * 
	 * @param orderId
	 * @param task
	 * @return
	 */
	
	public List<Status> getProcess(String orderId, String task) {
	
		List<Status> process = new ArrayList<Status>();
		Connection con;
		try {
			
			con = getConnection();
			logger.info("calling hist view:" + task); //$NON-NLS-1$
			HistSeqID hist = getHistSeqID(orderId, task);
			logger.info("HistId:" + hist.getHistSeqId()); //$NON-NLS-1$
			final String sql = "select Process_status_mnemonic from om_process_status " //$NON-NLS-1$
					+ "inner join om_task_status on om_process_status.process_status_id= om_task_status.process_status_id " //$NON-NLS-1$
					+ "inner join om_task on om_task_status.task_id = om_task.task_id " //$NON-NLS-1$
					+ "inner join om_order_flow on om_order_flow.task_id = om_task.task_id" //$NON-NLS-1$
					+ " where om_order_flow.order_seq_id=? and om_order_flow.hist_seq_id=?"; //$NON-NLS-1$
			final PreparedStatement prepareStmt = con.prepareStatement(sql);
			prepareStmt.setInt(1, Integer.parseInt(orderId));
			prepareStmt.setInt(2, Integer.parseInt(hist.getHistSeqId()));
			
			final ResultSet rs = prepareStmt.executeQuery();
			while (rs.next()) {
				
				String process_id = rs.getString("Process_status_mnemonic"); //$NON-NLS-1$
				logger.info("Process_status_mnemonic : " + process_id); //$NON-NLS-1$
				Status mapData = new Status();
				mapData.setProcess_status_mnemonic(process_id);
				process.add(mapData);
				
			}
		}
		catch (NumberFormatException e) {
			
			logger.error(e);
			throw new GenericAllException("{\"errorCode\":401,\"errorMsg\":\"HistId  not found!\"}"); //$NON-NLS-1$
			
		}
		catch (SQLException e) {
			
			logger.error(e);
			throw new GenericAllException(
					"{\"errorCode\":401,\"errorMsg\":\"Database Connection is  not found!\"}"); //$NON-NLS-1$
		}
		
		return process;
		
	}
	
	/**
	 * 
	 * @param tables
	 * @param joinList
	 * @param RJoinList
	 * @param groupList
	 * @param criteriaList
	 * @return
	 */
	public Data build(List<Table> tables, List<Join> joinList, List<Join> RJoinList,
			List<GroupBy> groupList, List<SearchCriteria> criteriaList) {
	
		SelectQuery sql = new SelectQuery();
		Data d = new Data();
		
		for (Table table : tables) {
			sql.addTable(table);
			
		}
		if (RJoinList != null)
			for (Join join : RJoinList) {
				sql.addRightOuterJoin(join.getTable1(), join.getTable1Colum1(), join.getTable2(),
						join.getTable2Colum2());
			}
		if (joinList != null)
			for (Join join : joinList) {
				sql.addJoin(join.getTable1(), join.getTable1Colum1(), join.getTable2(),
						join.getTable2Colum2());
				
			}
		if (groupList != null)
			for (GroupBy groupBy : groupList) {
				sql.addGroupByColumn(groupBy.getTable1(), groupBy.getColum());
				
			}
		if (criteriaList != null)
			for (SearchCriteria searchCriteria2 : criteriaList) {
				sql.addCriteria(searchCriteria2.getTable1(), searchCriteria2.getTable1Colum1(),
						searchCriteria2.getOpertor(), searchCriteria2.getValues());
			}
		
		String queryString = sql.toString();
		
		try {
			final Connection con = getConnection();
			
			final Statement stmnt = con.createStatement();
			
			ResultSet rs = stmnt.executeQuery(sql.toString());
			while (rs.next()) {
				for (Table table : tables) {
					List<String> parametrs = table.getColumnsWithAlias();
					for (String string : parametrs) {
						String view = rs.getString(string);
						d.setVaraible(view);
					}
					
				}
				
			}
			con.close();
		}
		catch (SQLException e) {
			logger.error(e);
			
		}
		
		return d;
	}
	
	/**
	 * 
	 * @return
	 */
	
	public List<OrderStatus> getOrderState() {
	
		List<OrderStatus> process = new ArrayList<OrderStatus>();
		final String sql = "SELECT ID,MNEMONIC as ORDERSTATE from om_ospolicy_state"; //$NON-NLS-1$
		
		ResultSet rs;
		try {
			
			rs = getConnection().prepareStatement(sql).executeQuery(sql);
			while (rs.next()) {
				
				String process_id = rs.getString("ID"); //$NON-NLS-1$
				String state = rs.getString(Messages.getString("DB_Connection.73")); //$NON-NLS-1$
				logger.info("id : " + process_id + ", state :" + state); //$NON-NLS-1$ //$NON-NLS-2$
				
				OrderStatus mapData = new OrderStatus();
				mapData.setId(process_id);
				mapData.setOrderstate(state);
				process.add(mapData);
				
			}
		}
		catch (SQLException e) {
			
			logger.error(e);
			throw new GenericAllException(
					"{\"errorCode\":401,\"errorMsg\":\"DataBase Connection  not found!\"}"); //$NON-NLS-1$
			
		}
		
		return process;
		
	}
	
	/**
	 * 
	 * @return
	 */
	
	public JSONArray getUsers() {
	
		final String sql = "select username from om_user_workgroup"; //$NON-NLS-1$
		JSONArray jArray = new JSONArray();
		
		ResultSet rs;
		try {
			
			rs = getConnection().prepareStatement(sql).executeQuery(sql);
			while (rs.next()) {
				
				String username = rs.getString("username"); //$NON-NLS-1$
				logger.info("UserName :" + username); //$NON-NLS-1$
				JSONObject jObject = new JSONObject();
				jObject.put("username", username); //$NON-NLS-1$
				jArray.put(jObject);
				
			}
		}
		catch (SQLException e) {
			
			logger.error(e);
			throw new GenericAllException(
					"{\"errorCode\":401,\"errorMsg\":\"DataBase Connection  not found!\"}"); //$NON-NLS-1$
			
		}
		catch (JSONException e) {
			
			logger.error(e);
		}
		
		return jArray;
		
	}
	
	/**
	 * 
	 * @return list of Work group
	 */
	
	public JSONArray getWorkgroups() {
	
		final String sql = "select workgroup_mnemonic as workgroup from om_workgroups"; //$NON-NLS-1$
		JSONArray jArray = new JSONArray();
		
		ResultSet rs;
		try {
			
			rs = getConnection().prepareStatement(sql).executeQuery(sql);
			while (rs.next()) {
				
				String workgroup = rs.getString("workgroup"); //$NON-NLS-1$
				logger.info("workgroup :" + workgroup); //$NON-NLS-1$
				JSONObject jObject = new JSONObject();
				jObject.put("workgroup", workgroup); //$NON-NLS-1$
				jArray.put(jObject);
				
			}
		}
		catch (SQLException e) {
			
			logger.error(e);
			throw new GenericAllException(
					"{\"errorCode\":401,\"errorMsg\":\"DataBase Connection  not found!\"}"); //$NON-NLS-1$
			
		}
		catch (JSONException e) {
			logger.error(e);
		}
		
		return jArray;
		
	}
	
	/**
	 * This method will return List of tasks from OSM Database
	 * 
	 * @return
	 */
	public JSONArray getTask() {
	
		final String sql = "select task_mnemonic as task from om_task"; //$NON-NLS-1$
		JSONArray jArray = new JSONArray();
		
		ResultSet rs;
		try {
			
			rs = getConnection().prepareStatement(sql).executeQuery(sql);
			while (rs.next()) {
				
				String task = rs.getString("task"); //$NON-NLS-1$
				logger.info("task :" + task); //$NON-NLS-1$
				JSONObject jObject = new JSONObject();
				jObject.put("task", task); //$NON-NLS-1$
				jArray.put(jObject);
				
			}
		}
		catch (SQLException e) {
			
			logger.error(e);
			throw new GenericAllException(
					"{\"errorCode\":401,\"errorMsg\":\"DataBase Connection  not found!\"}"); //$NON-NLS-1$
			
		}
		catch (JSONException e) {
			logger.error(e);
		}
		
		return jArray;
		
	}
	
	/**
	 * This Method will return list of Order Source from OSM DataBase
	 * 
	 * @return
	 */
	
	public JSONArray getSource() {
	
		final String sql = "select order_source_mnemonic as source from om_order_source"; //$NON-NLS-1$
		JSONArray jArray = new JSONArray();
		
		ResultSet rs;
		try {
			
			rs = getConnection().prepareStatement(sql).executeQuery(sql);
			while (rs.next()) {
				
				String source = rs.getString("source"); //$NON-NLS-1$
				logger.info("source :" + source); //$NON-NLS-1$
				JSONObject jObject = new JSONObject();
				jObject.put("source", source); //$NON-NLS-1$
				jArray.put(jObject);
				
			}
		}
		catch (SQLException e) {
			
			logger.error(e);
			throw new GenericAllException(
			
			Messages.getString("DB_Connection.1")); //$NON-NLS-1$
			
		}
		catch (JSONException e) {
			logger.error(e);
		}
		
		return jArray;
		
	}
	
	/**
	 * This method will return List of Order TYpes in OSM
	 * 
	 * @return
	 */
	public JSONArray getType() {
	
		final String sql = "select order_type_mnemonic as orderType from om_order_type"; //$NON-NLS-1$
		JSONArray jArray = new JSONArray();
		ResultSet rs;
		try {
			
			rs = getConnection().prepareStatement(sql).executeQuery(sql);
			while (rs.next()) {
				
				String orderType = rs.getString("orderType"); //$NON-NLS-1$
				logger.info("orderType :" + orderType); //$NON-NLS-1$
				JSONObject jObject = new JSONObject();
				jObject.put("orderType", orderType); //$NON-NLS-1$
				jArray.put(jObject);
				
			}
		}
		catch (SQLException e) {
			
			logger.error(e);
			throw new GenericAllException(
					"{\"errorCode\":401,\"errorMsg\":\"DataBase Connection  not found!\"}"); //$NON-NLS-1$
			
		}
		catch (JSONException e) {
			
			logger.error(e);
			throw new GenericAllException(
					"{\"errorCode\":401,\"errorMsg\":\"Json Exception Occured!\"}"); //$NON-NLS-1$
		}
		
		return jArray;
		
	}
	
	public JSONArray reportForOrderState() {
	
		final String sql = "select count(ord_state_id) as total,Mnemonic from om_ospolicy_state "
				+ "inner join om_order_header on om_ospolicy_state.id = om_order_header.ord_state_id "
				+ "group by om_ospolicy_state.Mnemonic"; //$NON-NLS-1$
		JSONArray jArray = new JSONArray();
		ResultSet rs;
		try {
			
			rs = getConnection().prepareStatement(sql).executeQuery(sql);
			while (rs.next()) {
				
				String count = rs.getString("total");
				String mnemonic = rs.getString("Mnemonic");//$NON-NLS-1$
				
				JSONObject jObject = new JSONObject();
				jObject.put("total", count);
				jObject.put("mnemonic", mnemonic);
				jArray.put(jObject);
				
			}
		}
		catch (SQLException e) {
			
			logger.error(e);
			throw new GenericAllException(
					"{\"errorCode\":401,\"errorMsg\":\"DataBase Connection  not found!\"}"); //$NON-NLS-1$
			
		}
		catch (JSONException e) {
			
			logger.error(e);
			throw new GenericAllException(
					"{\"errorCode\":401,\"errorMsg\":\"Json Exception Occured!\"}"); //$NON-NLS-1$
		}
		
		return jArray;
		
	}
	
	public JSONArray roleBasedPendingOrder() {
	
		final String sql = "select count(om_workgroups.workgroup_mnemonic) as"
				+ " Total,om_cartridge.namespace_mnemonic,om_cartridge.version from om_workgroup_order_view "
				+ "inner join om_workgroups on om_workgroups.workgroup_id = om_workgroup_order_view.workgroup_id "
				+ "inner join om_order_header on om_order_header.cartridge_id = om_workgroup_order_view.cartridge_id "
				+ "inner join om_cartridge on om_workgroup_order_view.cartridge_id = om_cartridge.cartridge_id "
				+ "where om_order_header.ord_state_id != '7' and om_order_header.ord_state_id!= '9' and om_order_header.ord_state_id != '3' "
				+ "group by om_workgroups.workgroup_mnemonic,om_cartridge.namespace_mnemonic,om_cartridge.version";
		JSONArray jArray = new JSONArray();
		ResultSet rs;
		try {
			
			rs = getConnection().prepareStatement(sql).executeQuery(sql);
			while (rs.next()) {
				String count = rs.getString("Total");
				String mnemonic = rs.getString("namespace_mnemonic");
				String version = rs.getString("version");
				
				JSONObject jObject = new JSONObject();
				jObject.put("total", count);
				jObject.put("mnemonic", mnemonic);
				jObject.put("version", version);
				jArray.put(jObject);
				
			}
		}
		catch (SQLException e) {
			
			logger.error(e);
			throw new GenericAllException(
					"{\"errorCode\":401,\"errorMsg\":\"DataBase Connection  not found!\"}");
			
		}
		catch (JSONException e) {
			
			logger.error(e);
			throw new GenericAllException(
					"{\"errorCode\":401,\"errorMsg\":\"Json Exception Occured!\"}");
		}
		
		return jArray;
		
	}
	
}
