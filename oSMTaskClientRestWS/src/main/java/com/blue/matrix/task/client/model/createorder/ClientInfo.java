
package com.blue.matrix.task.client.model.createorder;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "client_info")
public class ClientInfo {
	
	public void setName(String name) {
	
		this.name = name;
	}
	
	public void setAddress(Address address) {
	
		this.address = address;
	}
	
	@XmlElement(name = "name")
	private String name;
	
	@XmlElement(name = "address")
	private Address address;
	
}
